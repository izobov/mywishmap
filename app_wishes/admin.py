from django.contrib import admin
from app_wishes.models import *


@admin.register(Area)
class AreaAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'profile', )
    list_display_links = ('id', 'title', )
    list_filter = ('profile', )


@admin.register(Fear)
class FearAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'wish', 'neutralized',)
    list_display_links = ('title', 'wish',)


@admin.register(Neutralization)
class NeutralizationAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'fear', 'n_type',)
    list_display_links = ('title', 'fear', 'n_type',)
    list_filter = ('fear',)


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'is_partner', 'wishes_count', 'feedback_value', 'subscription', 'leader', )
    list_display_links = ('user',)
    list_filter = ('is_partner', 'leader', )
    search_fields = ['user__username', ]


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'is_active', 'autopay', 'tariff', 'expired_date', )
    list_display_links = ('id', 'tariff', )
    list_filter = ('tariff', 'autopay', )


@admin.register(Tariff)
class TariffAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'period', 'price', 'is_renewable')
    list_display_links = ('id', 'title', )


@admin.register(ToDo)
class ToDoAdmin(admin.ModelAdmin):
    list_display = ('id', 'wish', 'title', 'is_checked', )
    list_display_links = ('title', )
    list_filter = ('wish', )


@admin.register(Wish)
class WishAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'area', 'profile', 'importance', )
    list_display_links = ('id', 'title', )
    list_filter = ('profile', 'area', 'importance', )


@admin.register(Promocode)
class PromocodeAdmin(admin.ModelAdmin):
    list_display = ('id', 'promocode', 'leader', 'discount', )
    list_display_links = ('id', 'promocode', )
    list_filter = ('discount', 'leader', )


@admin.register(UserAction)
class UserActionAdmin(admin.ModelAdmin):
    list_display = ('id', 'date', 'email', 'amount', 'information', )
    list_display_links = ('id', 'date', )
    list_filter = ('email', 'amount', )
