import calendar
import csv
import datetime
import logging
import re
import traceback
import urllib
from random import randint
from PIL import Image
from yookassa import Configuration, Payment

import requests
from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, \
    PasswordResetCompleteView
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.db.models import Q
from django.forms import modelformset_factory, ModelForm
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.views import View

from app_wishes.models import *
from app_wishes.forms import *

logger = logging.getLogger(__name__)


# ЭКРАНЫ ===============================================================================================

def landing_page(request):
    if request.user.is_authenticated:
        return redirect('homepage')

    request.session['actual_page'] = 'LP'
    # promocode = promocode

    # ДАННЫЕ ДЛЯ БЛОКА "ОТЗЫВЫ"
    profiles = Profile.objects.filter(feedback_value__gte=4)
    if not profiles.first():
        profiles = None

    # ДАННЫЕ ДЛЯ БЛОКА "ИНДИКАТОРЫ"
    maps_count = Profile.objects.all().count() + 200
    maps_count_ending = str(maps_count)[-1]
    if maps_count_ending == '1':
        maps_count_ending = 'ель'
    elif maps_count_ending == '2' or maps_count_ending == '3' or maps_count_ending == '4':
        maps_count_ending = 'я'
    else:
        maps_count_ending = 'ей'

    wishes_count = maps_count * 39
    fears_count = round(wishes_count * 0.08)
    archived_wishes_count = round(wishes_count * 0.17)

    everyday_users_count = round(maps_count * 0.43)
    everyday_users_count_ending = str(everyday_users_count)[-1]
    if everyday_users_count_ending == '1':
        everyday_users_count_ending = 'ель'
    elif everyday_users_count_ending == '2' or everyday_users_count_ending == '3' or everyday_users_count_ending == '4':
        everyday_users_count_ending = 'я'
    else:
        everyday_users_count_ending = 'ей'

    context = {
        # ДАННЫЕ
        # 'promocode': promocode,
        'profiles': profiles,
        'maps_count': maps_count,
        'wishes_count': wishes_count,
        'fears_count': fears_count,
        'archived_wishes_count': archived_wishes_count,
        'maps_count_ending': maps_count_ending,
        'everyday_users_count': everyday_users_count,
        'everyday_users_count_ending': everyday_users_count_ending,
        # ФЛАГИ
        'actual_page': request.session['actual_page'],
        # ФОРМЫ
    }
    return render(request, 'lp/page__lp.html', context)


def registration_page(request):
    if request.user.is_authenticated:
        return redirect('homepage')

    request.session['actual_page'] = 'LP'
    # promocode = promocode
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.email = user.username
            user.save()

            # leader_pk = re.sub('.*?([0-9]*)$', r'\1', promocode)
            # try:
            #     leader = Profile.objects.filter(pk=leader_pk).first()
            # except Exception:
            #     leader = None

            profile = Profile.objects.create(
                user=user,
                # leader=leader,
            )
            profile.initial_filling()
            login(request, user)
            return redirect('homepage')
    context = {
        # ДАННЫЕ
        # 'promocode': promocode,
        # ФЛАГИ
        'actual_page': request.session['actual_page'],
        # ФОРМЫ
        'form': form,
    }
    return render(request, 'app_users/page__registration.html', context)


def login_page(request):
    if request.user.is_authenticated:
        return redirect('homepage')

    request.session['actual_page'] = 'LOGIN_PAGE'
    # promocode = promocode
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('homepage')
    context = {
        # ДАННЫЕ
        # 'promocode': promocode,
        # ФЛАГИ
        'actual_page': request.session['actual_page'],
        # ФОРМЫ
        'form': form,
    }
    return render(request, 'app_users/page__login.html', context)


def blocked_page(request):
    request.session['actual_page'] = 'BLOCKED_PAGE'
    profile = get_object_or_404(Profile, user=request.user)
    check_subscription_statuses(profile=profile)
    if profile.subscription.is_active:
        return redirect('wishmap')

    # promocode = promocode
    # leader = profile.leader
    # if not leader:
    #     if promocode == '-':
    #         leader = None
    #     else:
    #         try:
    #             leader_pk = re.sub('.*?([0-9]*)$', r'\1', promocode)
    #             leader = Profile.objects.filter(pk=leader_pk).first()
    #             profile.leader = leader
    #             profile.save()
    #         except Exception:
    #             messages.error(request, 'Промокод не найден')
    #             leader = None

    # form = PromocodeForm()
    # if request.method == 'POST':
    #     form = PromocodeForm(request.POST)
    #     if form.is_valid():
    #         promocode = str(form.cleaned_data.get('promocode')).strip()
    #         return redirect('blocked_page', promocode)

    countdown_hours = 0
    countdown_minutes = 0
    countdown_seconds = 0
    countdown = profile.get_countdown()
    if countdown:
        countdown_seconds = countdown.seconds % 60
        countdown_minutes = 60 - countdown.seconds // 60 % 60
        countdown_hours = 23 - countdown.seconds // 3600

    context = {
        # ДАННЫЕ
        # 'promocode': promocode,
        'profile': profile,
        # 'leader': leader,
        'countdown': countdown,
        'countdown_seconds': countdown_seconds,
        'countdown_minutes': countdown_minutes,
        'countdown_hours': countdown_hours,
        # ФЛАГИ
        'actual_page': request.session['actual_page'],
        # ФОРМЫ
        # 'form': form,
    }
    return render(request, 'app_users/page__blocked.html', context)


def payment_page(request, cost):
    request.session['actual_page'] = 'PAYMENT_PAGE'
    # cost 49, 99, 199, 2990, 3990
    if cost == 1 or cost == 49 or cost == 99 or cost == 199:
        tariff = get_object_or_404(Tariff, pk=1)
    else:
        tariff = get_object_or_404(Tariff, pk=2)

    payment_value = cost
    payment_currency = 'RUB'
    payment_description = f'ВИЖН. Подписка – {tariff.title}'

    # request.session['payment_value'] = payment_value

    Configuration.account_id = settings.YOOKASSA_ACCOUNT_ID
    Configuration.secret_key = settings.YOOKASSA_SECRET_KEY

    # ЗАПРОС НА СОЗДАНИЕ ПЛАТЕЖА
    payment = dict(
        Payment.create(
            {
                "amount": {
                    "value": payment_value,
                    "currency": payment_currency,
                },
                "confirmation": {
                    "type": "embedded",
                },
                "capture": True,
                "save_payment_method": True,
                "description": payment_description,

                # --- ДАННЫЕ ДЛЯ ОТПРАВКИ ЧЕКА ---
                "receipt": {
                    "customer": {
                        "email": request.user.email,
                    },
                    "items": [
                        {
                            "description": payment_description,
                            "quantity": "1",
                            "amount": {
                                "value": payment_value,
                                "currency": payment_currency,
                            },
                            "vat_code": "1"
                        },
                    ]
                }
                # --- КОНЕЦ ---
            }
        )
    )

    payment_id = payment['id']
    confirmation_token = payment['confirmation']['confirmation_token']

    context = {
        # ДАННЫЕ
        'payment_id': payment_id,
        'confirmation_token': confirmation_token,
        'payment_description': payment_description,
        'payment_value': payment_value,
        'payment_currency': payment_currency,
        'return_url': settings.RETURN_URL,
        # ФЛАГИ
        'actual_page': request.session['actual_page'],
        # ФОРМЫ
    }

    # Сохранение данных
    profile = get_object_or_404(Profile, user=request.user)
    if not profile.subscription:
        profile.subscription = Subscription.objects.create()

    profile.subscription.tariff = tariff
    profile.subscription.payment_id = payment_id
    profile.subscription.payment_status = 'pending'
    profile.subscription.save()

    return render(request, 'app_sales/payment.html', context)


def set_last_month(request):
    """Процедура установки прошлого месяца в качестве активного"""
    profile = Profile.objects.get(user=request.user)
    profile.set_last_month()
    return redirect('partnership_page')


def set_next_month(request):
    """Процедура установки следующего месяца в качестве активного"""
    profile = Profile.objects.get(user=request.user)
    profile.set_next_month()
    return redirect('partnership_page')


def partnership_page(request):
    profile = Profile.objects.get(user=request.user)
    if not profile.is_partner:
        return redirect('homepage')
    request.session['actual_page'] = 'PARTNERSHIP_PAGE'

    start_date = profile.active_month_start_date
    end_date = profile.get_month_end_date(profile.active_month_start_date)
    today = datetime.date.today()

    month_type = 1  # Текущий месяц
    if today > end_date:
        month_type = 0  # Прошлый месяц
    if today < start_date:
        month_type = 2  # Будущий месяц

    followers = Profile.objects.filter(leader=profile)

    # ПОЛЬЗОВАТЕЛИ
    # Вовлечено пользователей в текущем месяце
    current_month_followers_list = []
    for _ in followers:
        if start_date <= _.user.date_joined.date() <= end_date:
            current_month_followers_list.append(_.pk)
    current_month_followers = followers.filter(id__in=current_month_followers_list)
    current_month_followers_count = current_month_followers.count()
    # Совершили покупку в текущем месяце
    current_month_purchased_followers = current_month_followers.filter(subscription__is_active=True)
    current_month_purchased_followers_count = current_month_purchased_followers.count()
    # Оформили подписку в текущем месяце
    current_month_autopay_followers_count = current_month_purchased_followers.filter(subscription__autopay=True).count()
    # Общее количество вовлеченных пользователей
    followers_count = followers.count()
    # Общее количество подписчиков
    autopay_followers = followers.filter(
        subscription__is_active=True,
        subscription__autopay=True,
    )
    autopay_followers_count = autopay_followers.count()

    # ФИНАНСЫ
    # Сумма продаж
    paid_amount = 0
    if month_type == 0 or month_type == 1:
        followers_emails_list = []
        for _ in followers:
            followers_emails_list.append(_.user.email)
        payments = UserAction.objects.filter(
            date__gte=start_date,
            date__lte=end_date,
            email__in=followers_emails_list,
        )
        for _ in payments:
            paid_amount += _.amount
    # Сумма будущих платежей
    future_payments_amount = 0
    if month_type == 1:
        future_payments_followers = followers.filter(
            subscription__is_active=True,
            subscription__autopay=True,
            subscription__renewal_date__gt=today,
            subscription__renewal_date__lte=end_date,
        )
        for _ in future_payments_followers:
            future_payments_amount += _.subscription.tariff.price
    if month_type == 2:
        future_payments_followers = followers.filter(
            subscription__is_active=True,
            subscription__autopay=True,
            subscription__renewal_date__gt=start_date,
            subscription__renewal_date__lte=end_date,
        )
        for _ in future_payments_followers:
            future_payments_amount += _.subscription.tariff.price
    # Итог месяца
    month_result = paid_amount + future_payments_amount
    # Партнерское вознаграждение
    salary = 0
    if month_type == 0:
        salary = int(paid_amount * 0.4)
    if month_type == 1:
        salary = int(month_result * 0.4)
    if month_type == 2:
        salary = int(future_payments_amount * 0.4)

    # Отображение кнопки НАЗАД
    show_set_last_month_button = True
    if start_date < profile.user.date_joined.date():
        show_set_last_month_button = False

    # Отображение кнопки ВПЕРЕД
    show_set_next_month_button = False
    last_renewal_date = today
    for _ in autopay_followers:
        if _.subscription.renewal_date > last_renewal_date:
            last_renewal_date = _.subscription.renewal_date
    if last_renewal_date > today:
        if end_date < last_renewal_date:
            show_set_next_month_button = True
    else:
        if end_date < today:
            show_set_next_month_button = True

    invite_code = profile.get_invite_code()
    invite_link = f'{settings.INVITE_URL}{invite_code}'

    context = {
        'profile': profile,
        'month_type': month_type,
        # Пользователи
        'current_month_followers_count': current_month_followers_count,
        'current_month_purchased_followers_count': current_month_purchased_followers_count,
        'current_month_autopay_followers_count': current_month_autopay_followers_count,
        'followers_count': followers_count,
        'autopay_followers_count': autopay_followers_count,
        # Финансы
        'paid_amount': paid_amount,                         # Сумма продаж
        'future_payments_amount': future_payments_amount,   # Сумма будущих платежей
        'month_result': month_result,                       # Итог месяца
        'salary': salary,                                   # Партнерское вознаграждение
        'show_set_last_month_button': show_set_last_month_button,
        'show_set_next_month_button': show_set_next_month_button,
        'invite_code': invite_code,
        'invite_link': invite_link,
    }
    return render(request, 'app_partners/page__pertnership.html', context)


def followers_page(request):
    request.session['actual_page'] = 'FOLLOWERS_PAGE'
    profile = Profile.objects.get(user=request.user)
    followers = Profile.objects.filter(leader=profile)
    followers_count = followers.count()
    context = {
        'followers': followers,
        'followers_count': followers_count,
    }
    return render(request, 'app_partners/page__followers.html', context)


def change_wishmap_mode(request):
    profile = get_object_or_404(Profile, user=request.user)
    profile.visionmap_mode = not profile.visionmap_mode
    profile.save()
    if profile.visionmap_mode:
        messages.success(request, 'Режим вижн-карты включен')
    else:
        messages.success(request, 'Режим вижн-карты отключен')
    return redirect('profile')


def my_products_page(request):
    context = {}
    return render(request, 'page__my_products.html', context)


def statistics_page(request):
    profile = get_object_or_404(Profile, user=request.user)
    if profile.pk != 1:
        return redirect('homepage')
    today = datetime.date.today()
    # ТЕКУЩИЙ МЕСЯЦ
    current_month_days_count = calendar.monthrange(today.year, today.month)[1]
    current_month_start_date = datetime.date.today().replace(day=1)
    current_month_end_date = current_month_start_date + datetime.timedelta(days=(current_month_days_count - 1))
    # СЛЕДУЮЩИЙ МЕСЯЦ
    next_month_start_date = current_month_end_date + datetime.timedelta(days=1)
    next_month_days_count = calendar.monthrange(next_month_start_date.year, next_month_start_date.month)[1]
    next_month_end_date = next_month_start_date + datetime.timedelta(days=(next_month_days_count - 1))
    # СТАТИСТИКА
    current_month_registered_users = Profile.objects.filter(
        user__date_joined__gte=current_month_start_date,
        user__date_joined__lte=current_month_end_date
    )

    # Количество новых пользователей ==================================================================================
    current_month_registered_users_count = current_month_registered_users.count()
    # Из них, купили подписку
    current_month_registered_users_with_subscription_count = current_month_registered_users.filter(
        subscription__is_active=True,
    ).count()
    # Из них, с автопродлением
    current_month_registered_users_with_autopay_count = current_month_registered_users.filter(
        subscription__is_active=True,
        subscription__autopay=True,
    ).count()

    # Общее количество пользователей ==================================================================================
    users_count = Profile.objects.all().count()
    # Платящие пользователи
    paying_users = Profile.objects.filter(
        subscription__is_active=True,
        subscription__autopay=True
    )
    # Общее количество платящих пользователей
    paying_users_count = paying_users.count()

    # Из них живые
    live_users_period_start_date = datetime.date.today() - datetime.timedelta(days=7)
    live_users_period_end_date = datetime.date.today()
    live_users = Profile.objects.filter(
        subscription__is_active=True,
        subscription__autopay=True,

    )
    live_users_list = []
    for _ in live_users:
        if live_users_period_start_date <= _.user.last_login.date() <= live_users_period_end_date:
            live_users_list.append(_.pk)
    live_users = live_users.filter(id__in=live_users_list)
    live_users_count = live_users.count()

    # for live_user in live_users:
    #     live_user_last_login = live_user.user.last_login.date()
    #     if live_user_last_login < live_users_period_start_date or live_user_last_login > live_users_period_end_date:
    #         live_users.exclude(user=live_user.user)
    # live_users_count = live_users.count()
    # live_users_percent = int(live_users_count/users_count*100)
    # live_users_payments = live_users_count * 199

    # Общая сумма платежей
    total_payments_amount = 0
    for user in paying_users:
        total_payments_amount += user.subscription.tariff.price

    plan_percent = total_payments_amount / 150000 * 100

    # Сумма платежей в текущем месяце ================================================================================
    current_month_paying_users = paying_users.filter(
        subscription__renewal_date__gte=current_month_start_date,
        subscription__renewal_date__lte=current_month_end_date,
    )
    current_month_payments_amount = 0
    for user in current_month_paying_users:
        current_month_payments_amount += user.subscription.tariff.price
    # Сумма платежей в следующем месяце
    next_month_paying_users = paying_users.filter(
        subscription__renewal_date__gte=next_month_start_date,
        subscription__renewal_date__lte=next_month_end_date,
    )
    next_month_payments_amount = 0
    for user in next_month_paying_users:
        next_month_payments_amount += user.subscription.tariff.price

    paying_users_dict = {
        # date: дата платежа {
        #   amount: сумма платежа,
        #   usernames: пользователи,
        # }
    }

    for user in paying_users:
        date = user.subscription.renewal_date
        amount = user.subscription.tariff.price
        username = str(user.user.username)
        if date not in paying_users_dict:
            paying_users_dict[date] = {
                'amount': amount,
                'usernames': username,
            }
        else:
            paying_users_dict[date]['amount'] += amount
            paying_users_dict[date]['usernames'] += f', {username}'

    paying_users_dict = dict(sorted(paying_users_dict.items()))
    context = {
        # ТЕКУЩИЙ МЕСЯЦ
        'current_month_start_date': current_month_start_date,
        'current_month_end_date': current_month_end_date,
        'current_month_days_count': current_month_days_count,
        # СЛЕДУЮЩИЙ МЕСЯЦ
        'next_month_start_date': next_month_start_date,
        'next_month_end_date': next_month_end_date,
        'next_month_days_count': next_month_days_count,
        # СТАТИСТИКА
        'current_month_registered_users_count': current_month_registered_users_count,
        'current_month_registered_users_with_subscription_count': current_month_registered_users_with_subscription_count,
        'current_month_registered_users_with_autopay_count': current_month_registered_users_with_autopay_count,
        'users_count': users_count,
        'live_users': live_users,
        'live_users_count': live_users_count,
        'paying_users_count': paying_users_count,
        'total_payments_amount': total_payments_amount,
        'plan_percent': plan_percent,
        'current_month_payments_amount': current_month_payments_amount,
        'next_month_payments_amount': next_month_payments_amount,
        'paying_users_dict': paying_users_dict,
    }
    return render(request, 'page__statistics.html', context)


def check_subscription_statuses(profile):
    """
    Проверяет состояние подписок пользователей.
    Если profile указан, то только для него, иначе для всех.
    """
    profiles = Profile.objects.all()
    if profile:
        profiles.filter(profile=profile)
    else:
        if os.path.exists('subscription_date.txt'):
            with open('subscription_date.txt', 'r') as subscription_date_file:
                subscription_date = datetime.datetime.strptime(subscription_date_file.read(), '%Y-%m-%d').date()
                if subscription_date >= datetime.date.today():
                    return
        with open('subscription_date.txt', 'w') as subscription_date_file:
            subscription_date_str = '{:%Y-%m-%d}'.format(datetime.date.today())
            subscription_date_file.write(subscription_date_str)
    if profiles.first():
        for profile in profiles:
            profile.subscription.check_subscription_status()


def subscription_page(request):
    actual_page = 'subscription_page'
    request.session['actual_page'] = actual_page

    profile = get_object_or_404(Profile, user=request.user)
    check_subscription_statuses(profile=profile)
    if profile.subscription.is_active:
        return redirect('wishmap')

    # promocode_form = PromocodeForm()
    # if request.method == 'POST':
    #     promocode_form = PromocodeForm(request.POST)
    #     if promocode_form.is_valid():
    #         promocode = promocode_form.cleaned_data.get('promocode')
    #         return redirect('promocode_page', promocode=promocode)
    context = {
        'actual_page': actual_page,
        'profile': profile,
        # 'promocode_form': promocode_form,
    }
    return render(request, 'app_sales/page__subscription.html', context)


def homepage(request):
    if request.user.is_authenticated:
        profile = get_object_or_404(Profile, user=request.user)
        if profile.subscription.is_active:
            return redirect('wishmap')
        return redirect('blocked_page')
    return redirect('landing_page')


class WishMapView(View):
    """ВИЖН-карта"""

    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('homepage')

        # АКТУАЛЬНАЯ СТРАНИЦА
        actual_page = 'wishmap'
        request.session['actual_page'] = actual_page

        check_subscription_statuses(profile=None)

        profile = get_object_or_404(Profile, user=request.user)
        check_subscription_statuses(profile=profile)
        if not profile.subscription.is_active:
            return redirect('blocked_page', '-')

        # ФОРМА СОЗДАНИЯ ЖЕЛАНИЯ
        wish_create_form = WishTitleForm()

        # ФОРМЫ ОБЛАСТЕЙ
        area_create_form = AreaForm()
        area_edit_forms = {}
        areas = profile.areas()
        if areas:
            for area in areas:
                data = {
                    'title': area.title,
                    'color': area.color,
                }
                area_edit_forms[area.pk] = AreaForm(data=data)

        context = {
            # ДАННЫЕ
            'profile': profile,
            # ФЛАГИ
            'actual_page': actual_page,
            # ФОРМЫ
            'area_create_form': area_create_form,
            'area_edit_forms': area_edit_forms,
            'wish_create_form': wish_create_form,
        }
        if profile.pk == 962:
            return render(request, 'app_wishes/page__wishmap_new.html', context)
        return render(request, 'app_wishes/page__wishmap.html', context)

    def post(self, request):
        profile = get_object_or_404(Profile, user=request.user)
        if profile.areas():
            for area in profile.areas():
                # Создание желания
                submit_button_name = f'wish_create_{area.pk}'
                if request.POST.get(submit_button_name):
                    wish_create_form = WishTitleForm(request.POST)
                    if wish_create_form.is_valid():
                        wish = area.wish_create(
                            title=wish_create_form.cleaned_data.get('title')
                        )
                        return redirect(wish.get_absolute_url())
        # Создание области желаний
        if request.POST.get('area_save'):
            area_create_form = AreaForm(request.POST)
            if area_create_form.is_valid():
                profile.area_create(
                    title=area_create_form.cleaned_data.get('title'),
                    color=area_create_form.cleaned_data.get('color')
                )
        # Изменение областей
        if profile.areas():
            for area in profile.areas():
                # Сохранение области
                submit_button_name = f'area_{area.pk}_save'
                if request.POST.get(submit_button_name):
                    area_edit_form = AreaForm(request.POST)
                    if area_edit_form.is_valid():
                        area.title = area_edit_form.cleaned_data.get('title')
                        area.color = area_edit_form.cleaned_data.get('color')
                        area.save()
        return redirect('homepage')


class WishView(View):
    """Желание. Экран объекта"""

    def get(self, request, pk):
        if not request.user.is_authenticated:
            return redirect('homepage')

        # АКТУАЛЬНАЯ СТРАНИЦА
        actual_page = 'wish_page'
        request.session['actual_page'] = actual_page

        profile = get_object_or_404(Profile, user=request.user)
        wish = get_object_or_404(Wish, pk=pk)
        if wish.profile != profile:
            raise Http404()

        if profile.subscription.is_active:
            wish.set_progress()

        data = {
            'title': wish.title,
            'details': wish.details,
            'context': wish.context,
            'representation': wish.representation,
            'changes': wish.changes,
            'is_accepted': wish.is_accepted,
        }
        wish_title_form = WishTitleForm(data=data)
        wish_image_form = WishImageForm()
        wish_details_form = WishDetailsForm(data=data)
        wish_context_form = WishContextForm(data=data)
        wish_representation_form = WishRepresentationForm(data=data)
        wish_changes_form = WishChangesForm(data=data)
        wish_acceptance_form = WishAcceptanceForm(data=data)
        fear_create_form = FearTitleForm()

        # Форма создания действия
        data = {
            'title': '',
            'current_value': 0,
            'required_value': 0,
        }
        todo_create_form = TodoForm(data=data)

        # Формы редактирования действий
        todo_forms = {}
        todoes = wish.todoes()
        if todoes:
            for todo in todoes:
                data = {
                    'title': todo.title,
                    'current_value': todo.current_value,
                    'required_value': todo.required_value,
                }
                todo_forms[todo.pk] = TodoForm(data=data)

        context = {
            # ДАННЫЕ
            'wish': wish,
            'profile': profile,
            # ФЛАГИ
            'actual_page': actual_page,
            # ФОРМЫ
            'wish_title_form': wish_title_form,
            'wish_image_form': wish_image_form,
            'wish_details_form': wish_details_form,
            'wish_context_form': wish_context_form,
            'wish_representation_form': wish_representation_form,
            'wish_changes_form': wish_changes_form,
            'wish_acceptance_form': wish_acceptance_form,
            'fear_create_form': fear_create_form,
            'todo_create_form': todo_create_form,
            'todo_forms': todo_forms,
        }
        return render(request, 'app_wishes/page__wish.html', context)

    def post(self, request, pk):
        wish = get_object_or_404(Wish, pk=pk)
        if request.method == 'POST':
            # Сохранение наименования желания
            if request.POST.get("wish_title_save"):
                wish_title_form = WishTitleForm(request.POST)
                if wish_title_form.is_valid():
                    wish.title = wish_title_form.cleaned_data.get('title')
                    wish.save()
            # Удаление изображения
            if request.POST.get("wish_image_delete"):
                wish.image_delete()
                wish.save()
            # Сохранение изображения
            if request.POST.get("wish_image_save"):
                wish_image_form = WishImageForm(request.POST, request.FILES)
                if wish_image_form.is_valid():
                    try:
                        image_file = wish_image_form.cleaned_data.get('image')
                        image_url = wish_image_form.cleaned_data.get('url')
                        # Если пользователь выбрал изображение ...
                        if image_file:
                            # Валидация
                            if str(image_file).lower().endswith('.jpg') or str(image_file).lower().endswith(
                                    '.jpeg') or str(image_file).lower().endswith('.png'):
                                wish.image_delete()
                                wish.image = image_file
                                wish.save()
                                wish.image_transformation()
                            else:
                                messages.error(request, 'ОШИБКА! Недопустимый формат файла')
                        # Если пользователь прикрепил ссылку ...
                        if image_url != '':
                            # Валидация
                            if image_url.lower().endswith('.jpg') or image_url.lower().endswith(
                                    '.jpeg') or image_url.lower().endswith('.png'):
                                wish.image_delete()
                                content = requests.get(image_url).content
                                temp_file = NamedTemporaryFile(delete=True)
                                temp_file.write(content)
                                temp_file.flush()
                                file_name = ''
                                if image_url[-4:].startswith('.'):
                                    file_name = f'{randint(0, 999999)}{image_url[-4:]}'
                                elif image_url[-5:].startswith('.'):
                                    file_name = f'{randint(0, 999999)}{image_url[-5:]}'
                                wish.image.save(file_name, File(temp_file))
                                wish.save()
                                wish.image_transformation()
                            else:
                                messages.error(request, 'ОШИБКА! Недопустимая ссылка')
                    except Exception as exc:
                        trace_back = traceback.format_exc()
                        logger.error(f'{str(exc)} – {str(trace_back)}')
                        messages.error(request, 'Ошибка загрузки файла')
                        wish.image_delete()
            # Сохранение подробностей
            if request.POST.get("wish_details_save"):
                wish_details_form = WishDetailsForm(request.POST)
                if wish_details_form.is_valid():
                    wish.details = wish_details_form.cleaned_data.get('details')
                    wish.save()
            # Сохранение осознания
            if request.POST.get("wish_context_save"):
                wish_context_form = WishContextForm(request.POST)
                if wish_context_form.is_valid():
                    wish.context = wish_context_form.cleaned_data.get('context')
                    wish.save()
            # Сохранение воспоминаний
            if request.POST.get("wish_representation_save"):
                wish_representation_form = WishRepresentationForm(request.POST)
                if wish_representation_form.is_valid():
                    wish.representation = wish_representation_form.cleaned_data.get('representation')
                    wish.save()
            # Сохранение изменений
            if request.POST.get("wish_changes_save"):
                wish_changes_form = WishChangesForm(request.POST)
                if wish_changes_form.is_valid():
                    wish.changes = wish_changes_form.cleaned_data.get('changes')
                    wish.save()
            # Создание опасения
            if request.POST.get("fear_save"):
                fear_create_form = FearTitleForm(request.POST)
                if fear_create_form.is_valid():
                    fear_title = fear_create_form.cleaned_data.get('title')
                    Fear.objects.create(title=fear_title, wish=wish)
                    return redirect(wish.get_fears_list_url())
            # Сохранение принятия
            if request.POST.get("wish_acceptance_save"):
                wish_acceptance_form = WishAcceptanceForm(request.POST)
                if wish_acceptance_form.is_valid():
                    wish.is_accepted = wish_acceptance_form.cleaned_data.get('is_accepted')
                    wish.save()
            # ДЕЙСТВИЯ
            # Создание действия
            if request.POST.get("todo_save"):
                todo_form = TodoForm(request.POST)
                if todo_form.is_valid():
                    title = todo_form.cleaned_data.get('title')
                    current_value = 0
                    if todo_form.cleaned_data.get('current_value'):
                        current_value = todo_form.cleaned_data.get('current_value')
                    required_value = 0
                    if todo_form.cleaned_data.get('required_value'):
                        required_value = todo_form.cleaned_data.get('required_value')
                    is_checked = False
                    if required_value != 0 and required_value <= current_value:
                        is_checked = True
                    ToDo.objects.create(wish=wish, title=title, current_value=current_value,
                                        required_value=required_value, is_checked=is_checked)
            # Изменение действия
            todoes = wish.todoes()
            if todoes:
                # Сохранение действия
                for todo in todoes:
                    submit_button_name = f'todo_{todo.pk}_save'
                    if request.POST.get(submit_button_name):
                        todo_form = TodoForm(request.POST)
                        if todo_form.is_valid():
                            if todo_form.cleaned_data.get('title') == '':
                                todo.delete()
                            else:
                                todo.title = todo_form.cleaned_data.get('title')
                                todo.current_value = todo_form.cleaned_data.get('current_value')
                                todo.required_value = todo_form.cleaned_data.get('required_value')
                                if todo.current_value > todo.required_value:
                                    todo.current_value = 0
                                    todo.required_value = 0
                                if todo.required_value != 0 and todo.required_value == todo.current_value:
                                    todo.is_checked = True
                                todo.save()
                    # Удаление счетчика
                    submit_button_name = f'todo_{todo.pk}_clean_counter'
                    if request.POST.get(submit_button_name):
                        todo.current_value = 0
                        todo.required_value = 0
                        todo.save()
                    # Удаление действия
                    submit_button_name = f'todo_{todo.pk}_delete'
                    if request.POST.get(submit_button_name):
                        todo.delete()
        return redirect(wish.get_absolute_url())


class FearsListView(View):

    def get(self, request, pk):
        if not request.user.is_authenticated:
            return redirect('homepage')

        # АКТУАЛЬНАЯ СТРАНИЦА
        actual_page = 'fears_list'
        request.session['actual_page'] = actual_page

        wish = get_object_or_404(Wish, pk=pk)
        fear_create_form = FearTitleForm()
        context = {
            # ДАННЫЕ
            'wish': wish,
            # ФЛАГИ
            'actual_page': actual_page,
            # ФОРМЫ
            'fear_create_form': fear_create_form,
        }
        return render(request, 'app_wishes/page__fears_list.html', context)

    def post(self, request, pk):
        wish = get_object_or_404(Wish, pk=pk)
        # Создание опасения
        if request.POST.get("fear_save"):
            fear_create_form = FearTitleForm(request.POST)
            if fear_create_form.is_valid():
                wish = Wish.objects.get(pk=pk)
                fear_title = fear_create_form.cleaned_data.get('title')
                Fear.objects.create(title=fear_title, wish=wish)
        return redirect(wish.get_fears_list_url())


class FearView(View):
    """Опасение. Экран объекта"""

    def get(self, request, pk):
        if not request.user.is_authenticated:
            return redirect('homepage')

        # АКТУАЛЬНАЯ СТРАНИЦА
        actual_page = 'fear_page'
        request.session['actual_page'] = actual_page

        fear = get_object_or_404(Fear, pk=pk)
        fear.check_neutralizations_count()
        preventions = Neutralization.objects.filter(fear=fear, n_type=0)
        eliminations = Neutralization.objects.filter(fear=fear, n_type=1)
        used_preventions_count = Neutralization.objects.filter(fear=fear, n_type=0).exclude(title=None).count()
        used_eliminations_count = Neutralization.objects.filter(fear=fear, n_type=1).exclude(title=None).count()
        data = {
            'title': fear.title,
        }
        fear_title_edit_form = FearTitleForm(data=data)
        neutralization_create_form = NeutralizationTitleForm()
        neutralizations_title_formset = modelformset_factory(
            Neutralization,
            fields=('title',),
            form=NeutralizationTitleForm,
            extra=5,
            max_num=5,
        )
        preventions_formset = neutralizations_title_formset(queryset=preventions)
        eliminations_formset = neutralizations_title_formset(queryset=eliminations)
        context = {
            # ДАННЫЕ
            'fear': fear,
            'preventions': preventions,
            'eliminations': eliminations,
            'used_preventions_count': used_preventions_count,
            'used_eliminations_count': used_eliminations_count,
            # ФЛАГИ
            'actual_page': actual_page,
            # ФОРМЫ
            'fear_title_edit_form': fear_title_edit_form,
            'neutralization_create_form': neutralization_create_form,
            'preventions_formset': preventions_formset,
            'eliminations_formset': eliminations_formset,
        }
        return render(request, 'app_wishes/page__fear.html', context)

    def post(self, request, pk):
        fear = get_object_or_404(Fear, pk=pk)
        # Сохранение наименования опасения
        if request.POST.get("fear_title_save"):
            fear_title_edit_form = FearTitleForm(request.POST)
            if fear_title_edit_form.is_valid():
                fear.title = fear_title_edit_form.cleaned_data.get('title')
                fear.save()
        # Сохранение нейтрализаций
        if request.POST.get("neutralizations_save"):
            NeutralizationTitleFormset = modelformset_factory(Neutralization, fields=('title',), extra=5, max_num=5)
            neutralization_formset = NeutralizationTitleFormset(request.POST)
            if neutralization_formset.is_valid():
                neutralization_formset.save()
        # fear.set_neutralization_status()
        return redirect(fear.get_absolute_url())


class NeutralizationTitleForm(ModelForm):
    class Meta:
        model = Neutralization
        fields = ('title',)
        labels = {
            'title': ''
        }
        widgets = {
            'title': forms.Textarea(attrs={'class': 'form-control mt-1', 'rows': '2'}),
        }


class ToDoesListView(View):

    def get(self, request):
        # АКТУАЛЬНАЯ СТРАНИЦА
        actual_page = 'todoes_list'
        request.session['actual_page'] = actual_page

        if not request.user.is_authenticated:
            return redirect('homepage')

        profile = get_object_or_404(Profile, user=request.user)

        area_dict = {}
        todo_forms = {}
        for area in profile.areas():
            wishes = area.actual_wishes()
            if wishes:
                wishes_and_separators = []
                is_first_wish = True
                for wish in wishes:
                    todoes = wish.unchecked_todoes()
                    if todoes:
                        if not is_first_wish:
                            wishes_and_separators.append('separator')
                        wishes_and_separators.append(wish)
                        is_first_wish = False
                        # ФОРМЫ ИЗМЕНЕНИЯ ДЕЙСТВИЙ
                        for todo in todoes:
                            data = {
                                'title': todo.title,
                                'current_value': todo.current_value,
                                'required_value': todo.required_value,
                            }
                            todo_forms[todo.pk] = TodoForm(data=data)
                if wishes_and_separators:
                    area_dict[area] = wishes_and_separators

        context = {
            # ДАННЫЕ
            'profile': profile,
            'area_dict': area_dict,
            # ФЛАГИ
            'actual_page': actual_page,
            # ФОРМЫ
            'todo_forms': todo_forms,
        }
        return render(request, 'app_wishes/page__todoes_list.html', context)

    def post(self, request):
        profile = get_object_or_404(Profile, user=request.user)
        wishes = profile.actual_wishes()
        for wish in wishes:
            todoes = wish.todoes()
            if todoes:
                for todo in todoes:
                    # Сохранение изменений
                    submit_button_name = f'todo_{todo.pk}_save'
                    if request.POST.get(submit_button_name):
                        todo_form = TodoForm(request.POST)
                        if todo_form.is_valid():
                            if todo_form.cleaned_data.get('title') == '':
                                todo.delete()
                            else:
                                todo.title = todo_form.cleaned_data.get('title')
                                todo.current_value = todo_form.cleaned_data.get('current_value')
                                todo.required_value = todo_form.cleaned_data.get('required_value')
                                if todo.current_value > todo.required_value:
                                    todo.current_value = 0
                                    todo.required_value = 0
                                todo.save()
                    # Удаление действия
                    submit_button_name = f'todo_{todo.pk}_delete'
                    if request.POST.get(submit_button_name):
                        todo.delete()
                    # Удаление счетчика
                    submit_button_name = f'todo_{todo.pk}_clean_counter'
                    if request.POST.get(submit_button_name):
                        todo.current_value = 0
                        todo.required_value = 0
                        todo.save()
        return redirect('todoes_list')


# ФУНКЦИИ

# TODO: Отрефакторить всё, что ниже


def fear_delete(request, pk):
    """Опасение. Удаление объекта"""
    fear = get_object_or_404(Fear, pk=pk)
    wish = fear.wish
    fear.delete()
    messages.success(request, f'Опасение удалено')
    if wish.fears():
        return redirect(wish.get_fears_list_url())
    return redirect(wish.get_absolute_url())


def todo_change_checked(request, pk):
    """Действие. Изменение состояния"""
    # TODO: Отрефакторить
    todo = get_object_or_404(ToDo, pk=pk)
    todo.is_checked = not todo.is_checked
    if todo.is_counter:
        if todo.is_checked:
            todo.current_value = todo.required_value
        else:
            todo.current_value = 0
    todo.save()
    if request.session['actual_page'] == "todoes_list":
        return redirect('todoes_list')
    elif request.session['actual_page'] == "wish_page":
        return redirect(todo.wish.get_absolute_url())


def todo_rise_value(request, pk):
    """Действие. Повышение счетчика"""
    todo = get_object_or_404(ToDo, pk=pk)
    todo.current_value += 1
    if todo.current_value >= todo.required_value:
        todo.current_value = todo.required_value
        todo.is_checked = True
    todo.save()
    if request.session['actual_page'] == 'todoes_list':
        return redirect('todoes_list')
    elif request.session['actual_page'] == 'wish_page':
        return redirect(todo.wish.get_absolute_url())
    else:
        return redirect('homepage')


def wish_change_checked_todoes_view(request, pk):
    """Желание. Изменение отображения выполненных действий"""
    wish = get_object_or_404(Wish, pk=pk)
    wish.show_checked_todoes = not wish.show_checked_todoes
    wish.save()
    return redirect(wish.get_absolute_url())


class ArchivedWishesListView(View):

    def get(self, request):
        profile = get_object_or_404(Profile, user=request.user)
        # ОДНА КОЛОНКА ЖЕЛАНИЙ
        a_column_wishes = profile.archived_wishes()
        # ДВЕ КОЛОНКИ ЖЕЛАНИЙ
        b1_column_wishes = []
        b2_column_wishes = []
        marker = 1
        if a_column_wishes:
            for wish in a_column_wishes:
                if marker % 2 == 1:
                    b1_column_wishes.append(wish)
                else:
                    b2_column_wishes.append(wish)
                marker += 1
        # ТРИ КОЛОНКИ ЖЕЛАНИЙ
        c1_column_wishes = []
        c2_column_wishes = []
        c3_column_wishes = []
        marker = 1
        if a_column_wishes:
            for wish in a_column_wishes:
                if marker % 3 == 1:
                    c1_column_wishes.append(wish)
                elif marker % 3 == 2:
                    c2_column_wishes.append(wish)
                else:
                    c3_column_wishes.append(wish)
                marker += 1
        context = {
            'a_column_wishes': a_column_wishes,
            'b1_column_wishes': b1_column_wishes,
            'b2_column_wishes': b2_column_wishes,
            'c1_column_wishes': c1_column_wishes,
            'c2_column_wishes': c2_column_wishes,
            'c3_column_wishes': c3_column_wishes,
        }
        return render(request, 'app_wishes/page__archived_wishes.html', context)

    def post(self, request):
        pass


def area_move_up(request, pk, column):
    """Область. Сдвиг объекта на одну позицию вверх"""
    area = Area.objects.get(pk=pk)
    profile = area.profile
    areas_dict = json.loads(profile.areas_dict)
    column_list = areas_dict[column]
    area_index = column_list.index(area.pk)
    column_list[area_index], column_list[area_index - 1] = column_list[area_index - 1], column_list[area_index]
    areas_dict[column] = column_list
    profile.areas_dict = json.dumps(areas_dict)
    profile.save()
    return redirect('homepage')


def area_move_down(request, pk, column):
    """Область. Сдвиг объекта на одну позицию вниз"""
    area = Area.objects.get(pk=pk)
    profile = area.profile
    areas_dict = json.loads(profile.areas_dict)
    column_list = areas_dict[column]
    area_index = column_list.index(area.pk)
    column_list[area_index], column_list[area_index + 1] = column_list[area_index + 1], column_list[area_index]
    areas_dict[column] = column_list
    profile.areas_dict = json.dumps(areas_dict)
    profile.save()
    return redirect('homepage')


def area_move_left(request, pk, column):
    """Область. Перенос элемента в левую колонку"""
    area = Area.objects.get(pk=pk)
    profile = area.profile
    areas_dict = json.loads(profile.areas_dict)
    column_list = areas_dict[column]
    column_list.remove(area.pk)
    left_column = ''
    if column == 'b2':
        left_column = 'b1'
    elif column == 'c2':
        left_column = 'c1'
    elif column == 'c3':
        left_column = 'c2'
    left_column_list = areas_dict[left_column]
    left_column_list.append(area.pk)
    areas_dict[column] = column_list
    areas_dict[left_column] = left_column_list
    profile.areas_dict = json.dumps(areas_dict)
    profile.save()
    return redirect('homepage')


def area_move_right(request, pk, column):
    """Область. Перенос элемента в правую колонку"""
    area = Area.objects.get(pk=pk)
    profile = area.profile
    areas_dict = json.loads(profile.areas_dict)
    column_list = areas_dict[column]
    column_list.remove(area.pk)
    right_column = ''
    if column == 'b1':
        right_column = 'b2'
    elif column == 'c1':
        right_column = 'c2'
    elif column == 'c2':
        right_column = 'c3'
    right_column_list = areas_dict[right_column]
    right_column_list.append(area.pk)
    areas_dict[column] = column_list
    areas_dict[right_column] = right_column_list
    profile.areas_dict = json.dumps(areas_dict)
    profile.save()
    return redirect('homepage')


def area_delete(request, pk):
    """Область. Удаление объекта"""
    profile = get_object_or_404(Profile, user=request.user)
    result = profile.area_delete(pk)
    if result:
        messages.success(request, 'Область удалена')
    else:
        messages.error(request, 'Ошибка удаления области')
    return redirect('homepage')


def area_change_cols_count(request, pk):
    """Область. Изменение количества колонок желаний"""
    area = Area.objects.get(pk=pk)
    area.is_double = not area.is_double
    area.save()
    return redirect('homepage')


def wish_delete(request, pk):
    """Желание. Удаление объекта"""
    wish = get_object_or_404(Wish, pk=pk)
    result = wish.area.wish_delete(pk=wish.pk)
    if result:
        messages.success(request, 'Желание удалено')
    else:
        messages.error(request, 'Ошибка удаления желания')
    return redirect('homepage')


def wish_image_delete(request, pk):
    """Желание. Удаление изображения"""
    wish = get_object_or_404(Wish, pk=pk)
    wish.image_delete()
    wish.save()
    return redirect(wish.get_absolute_url())


def wish_change_archiving(request, pk):
    """Желание. Исполнение"""
    wish = get_object_or_404(Wish, pk=pk)
    wish.is_archived = not wish.is_archived
    wish.save()
    if not wish.is_archived:
        messages.success(request, f'Желание восстановлено')
    return redirect('homepage')


# # def check_subscriptions_status():
# #     file = open(settings.CHECK_SUBSCRIPTIONS_DATE_FILE, 'r')
# #     check_subscriptions_date = file.read()
# #     file.close()
# #     if date(check_subscriptions_date) == date.today():
# #         print('X' * 100)
# #         print(f'YES: {check_subscriptions_date}')
# #     else:
# #         file = open(settings.CHECK_SUBSCRIPTIONS_DATE_FILE, 'w')
# #         check_subscriptions_date = date.today()
# #         file.write(str(check_subscriptions_date))
# #         print('X' * 100)
# #         print(f'WRITE: {check_subscriptions_date}')
# #     # with open(settings.CHECK_SUBSCRIPTIONS_DATE_FILE) as file:
# #     #     check_subscriptions_date = file.read()


class ProfileView(View):  # ЗАВЕРШЕНО

    def get(self, request):
        if request.user.is_authenticated:
            profile = get_object_or_404(Profile, user=request.user)

            # АКТУАЛЬНАЯ СТРАНИЦА
            actual_page = 'profile_page'
            request.session['actual_page'] = actual_page

            # АВАТАР
            userpic_form = UserpicForm()

            # ИМЯ ПОЛЬЗОВАТЕЛЯ
            data = {'first_name': request.user.first_name, }
            username_form = UsernameForm(data=data)

            # ОЦЕНКА И ОТЗЫВ
            data = {'text': profile.feedback_text, }
            feedback_form = FeedbackForm(data=data)

            context = {
                # ДАННЫЕ
                'profile': profile,
                # ФЛАГИ
                'actual_page': actual_page,
                # ФОРМЫ
                'userpic_form': userpic_form,
                'username_form': username_form,
                'feedback_form': feedback_form,
            }
            return render(request, 'app_users/page__profile.html', context)
        else:
            return redirect('homepage')

    def post(self, request):
        profile = get_object_or_404(Profile, user=request.user)

        # Сохранение аватара
        if request.POST.get("userpic_save"):
            userpic_form = UserpicForm(request.POST, request.FILES)
            if userpic_form.is_valid():
                try:
                    image_file = userpic_form.cleaned_data.get('image')
                    image_url = userpic_form.cleaned_data.get('url')
                    # Если пользователь выбрал изображение ...
                    if image_file:
                        # Валидация
                        if str(image_file).lower().endswith('.jpg') or str(image_file).lower().endswith('.jpeg') or str(
                                image_file).lower().endswith('.png'):
                            profile.userpic_delete()
                            profile.userpic = image_file
                            profile.save()
                            profile.userpic_transformation()
                        else:
                            messages.error(request, 'ОШИБКА! Недопустимый формат файла')

                    # Если пользователь прикрепил ссылку ...
                    if image_url != '':
                        # Валидация
                        if image_url.lower().endswith('.jpg') or image_url.lower().endswith(
                                '.jpeg') or image_url.lower().endswith('.png'):
                            profile.userpic_delete()
                            content = requests.get(image_url).content
                            temp_file = NamedTemporaryFile(delete=True)
                            temp_file.write(content)
                            temp_file.flush()
                            file_name = ''
                            if image_url[-4:].startswith('.'):
                                file_name = f'{randint(0, 999999)}{image_url[-4:]}'
                            elif image_url[-5:].startswith('.'):
                                file_name = f'{randint(0, 999999)}{image_url[-5:]}'
                            profile.userpic.save(file_name, File(temp_file))
                            profile.save()
                            profile.userpic_transformation()
                        else:
                            messages.error(request, 'ОШИБКА! Недопустимая ссылка')

                except Exception as exc:
                    trace_back = traceback.format_exc()
                    logger.error(f'{str(exc)} – {str(trace_back)}')
                    messages.error(request, 'Неизвестная ошибка!')
                    profile.userpic_delete()

        # Сохранение имени пользователя
        if request.POST.get("username_save"):
            username_form = UsernameForm(request.POST)
            if username_form.is_valid():
                profile.user.first_name = username_form.cleaned_data.get('first_name')
                profile.user.save()

        # Сохранение отзыва
        # 1
        if request.POST.get("feedback_1_save"):
            feedback_form = FeedbackForm(request.POST)
            if feedback_form.is_valid():
                profile.feedback_value = 1
                profile.feedback_text = feedback_form.cleaned_data.get('text')
                profile.save()
        # 2
        if request.POST.get("feedback_2_save"):
            feedback_form = FeedbackForm(request.POST)
            if feedback_form.is_valid():
                profile.feedback_value = 2
                profile.feedback_text = feedback_form.cleaned_data.get('text')
                profile.save()
        # 3
        if request.POST.get("feedback_3_save"):
            feedback_form = FeedbackForm(request.POST)
            if feedback_form.is_valid():
                profile.feedback_value = 3
                profile.feedback_text = feedback_form.cleaned_data.get('text')
                profile.save()
        # 4
        if request.POST.get("feedback_4_save"):
            feedback_form = FeedbackForm(request.POST)
            if feedback_form.is_valid():
                profile.feedback_value = 4
                profile.feedback_text = feedback_form.cleaned_data.get('text')
                profile.save()
        # 5
        if request.POST.get("feedback_5_save"):
            feedback_form = FeedbackForm(request.POST)
            if feedback_form.is_valid():
                profile.feedback_value = 5
                profile.feedback_text = feedback_form.cleaned_data.get('text')
                profile.save()
        return redirect(profile.get_absolute_url())


# def registration_page(request):
#     form = RegistrationForm()
#     information = ''
#     if 'request_string' in request.session:
#         information = request.session.get('request_string')
#
#     if request.method == 'POST':
#         form = RegistrationForm(request.POST)
#         if form.is_valid():
#             user = form.save()
#             user.email = user.username
#             user.save()
#             profile = Profile.objects.create(
#                 user=user,
#                 information=information,
#             )
#             profile.save_action('Регистрация', information)
#             profile.initial_filling()
#             login(request, user)
#             return redirect('homepage')
#     context = {
#         'form': form,
#     }
#     return render(request, 'app_users/page__registration.html', context)





def logout_user(request):
    profile = get_object_or_404(Profile, user=request.user)
    # profile.save_action('Выход', '')
    logout(request)
    return redirect('homepage')


def userpic_delete(request):
    """Удаление аватара пользователя"""
    profile = get_object_or_404(Profile, user=request.user)
    profile.userpic_delete()
    return redirect(profile.get_absolute_url())


class UserPasswordResetView(PasswordResetView):
    form_class = ResetPasswordForm
    template_name = 'app_users/page__password_reset.html'


class UserPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'app_users/page__password_reset_done.html'


class UserPasswordResetConfirmView(PasswordResetConfirmView):
    form_class = ResetConfirmPasswordForm
    template_name = 'app_users/page__password_reset_confirm.html'


class UserPasswordResetCompleteView(PasswordResetCompleteView):
    template_name = 'app_users/page__password_reset_complete.html'


def password_change(request):
    user = request.user
    if request.method == 'POST':
        form = ChangePasswordForm(user, request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Пароль успешно изменен.')
            login(request, user)
            return redirect('homepage')
    else:
        form = ChangePasswordForm(user=user)
    return render(request, 'app_users/page__password_change.html', {'form': form})


def app_install_notification_close(request):
    """Отключение уведомления об установке приложения"""
    profile = get_object_or_404(Profile, user=request.user)
    profile.show_app_install_notification = False
    profile.save()
    return redirect('homepage')


def partnership_notification_close(request):
    """Отключение уведомления о партнерстве"""
    profile = get_object_or_404(Profile, user=request.user)
    profile.show_partnership_notification = False
    profile.save()
    return redirect('partnership_page')


def wish_info_notification_close(request, pk):
    """Отключение уведомления об информации"""
    wish = get_object_or_404(Wish, pk=pk)
    profile = wish.profile
    profile.show_wish_info_notification = False
    profile.save()
    return redirect(wish.get_absolute_url())


def page_404(request, exception):
    return render(request, 'page__404.html', status=404)


def page_500(request):
    return render(request, 'page__500.html', status=500)


def subscription_autopay_change(request):
    """Подписка. Изменение состояния автоплатежей"""
    profile = get_object_or_404(Profile, user=request.user)
    profile.subscription.autopay_change()
    if profile.subscription.autopay:
        message = 'Автопродление влючено'
    else:
        message = 'Автопродление отключено'
    messages.success(request, message)
    # profile.save_action(message, '')
    return redirect('profile')


def wish_set_importance(request, pk, value):
    wish = get_object_or_404(Wish, pk=pk)
    wish.importance = value
    wish.save()
    return redirect(wish.get_absolute_url())
