from django.urls import path
from app_wishes.views import *


urlpatterns = [
    # ДОМАШНЯЯ СТРАНИЦА
    path('', homepage, name='homepage'),
    path('404/', page_404, name='page_404'),
    path('500/', page_404, name='page_500'),

    # ВИЖН-КАРТА
    path('wishmap/', WishMapView.as_view(), name='wishmap'),

    # ОБЛАСТИ
    # Экраны
    # Функции
    path('area_move_up/<int:pk>/<str:column>', area_move_up, name='area_move_up'),
    path('area_move_down/<int:pk>/<str:column>', area_move_down, name='area_move_down'),
    path('area_move_left/<int:pk>/<str:column>', area_move_left, name='area_move_left'),
    path('area_move_right/<int:pk>/<str:column>', area_move_right, name='area_move_right'),
    path('area_delete/<int:pk>', area_delete, name='area_delete'),
    path('area_change_cols_count/<int:pk>', area_change_cols_count, name='area_change_cols_count'),

    # ЖЕЛАНИЯ
    # Экраны
    path('wish/<int:pk>', WishView.as_view(), name='wish_page'),
    # Функции
    path('wish_delete/<int:pk>', wish_delete, name='wish_delete'),
    path('wish_image_delete/<int:pk>', wish_image_delete, name='wish_image_delete'),
    path('wish_change_archiving/<int:pk>', wish_change_archiving, name='wish_change_archiving'),
    path('wish_set_importance/<int:pk>/<int:value>', wish_set_importance, name='wish_set_importance'),

    # СРАХИ И ОПАСЕНИЯ
    # Экраны
    path('fears/<int:pk>', FearsListView.as_view(), name='fears_list'),
    path('fear/<int:pk>', FearView.as_view(), name='fear_page'),
    # Функции
    path('fear_delete/<int:pk>', fear_delete, name='fear_delete'),

    # ИСПОЛНЕННОЕ
    # Экраны
    path('archivedwishes/', ArchivedWishesListView.as_view(), name='archived_wishes_list'),

    # ДЕЙСТВИЯ
    # Экраны
    path('todoes/', ToDoesListView.as_view(), name='todoes_list'),
    # Функции
    path('todo_change_checked/<int:pk>', todo_change_checked, name='todo_change_checked'),
    path('wish_change_checked_todoes_view/<int:pk>', wish_change_checked_todoes_view, name='wish_change_checked_todoes_view'),
    path('todo_rise_value/<int:pk>', todo_rise_value, name='todo_rise_value'),

    # ПРОФИЛЬ
    # Экраны
    path('profile/', ProfileView.as_view(), name='profile'),
    path('password_reset/', UserPasswordResetView.as_view(), name="reset_password"),
    path('password_reset_done/', UserPasswordResetDoneView.as_view(), name="password_reset_done"),
    path('password_reset_confirm/<uidb64>/<token>', UserPasswordResetConfirmView.as_view(), name="password_reset_confirm"),
    path('password_reset_complete/', UserPasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('password_change/', password_change, name='password_change'),
    # Функции
    path('logout/', logout_user, name='logout'),
    path('userpic_delete/', userpic_delete, name='userpic_delete'),
    path('app_install_notification_close/', app_install_notification_close, name='app_install_notification_close'),
    path('wish_info_notification_close/<int:pk>', wish_info_notification_close, name='wish_info_notification_close'),
    path('change_wishmap_mode/', change_wishmap_mode, name='change_wishmap_mode'),

    # ПОДПИСКА
    # Экраны
    path('payment/<int:cost>', payment_page, name='payment_page'),
    path('subscription/', subscription_page, name='subscription_page'),

    # Функции
    path('autopay_change', subscription_autopay_change, name='subscription_autopay_change'),
    
    # ПРОЧЕЕЕ
    path('my_products_page', my_products_page, name='my_products_page'),
    path('statistics_page', statistics_page, name='statistics_page'),

    # ЛЕНДИНГ, РЕГИСТРАЦИЯ, ВХОД, БЛОКИРОВКА
    # path('lp/<str:promocode>', landing_page, name='landing_page'),
    # path('registration/<str:promocode>', registration_page, name='registration_page'),
    # path('login/<str:promocode>', login_page, name='login_page'),
    # path('blocked/<str:promocode>', blocked_page, name='blocked_page'),
    path('lp/', landing_page, name='landing_page'),
    path('registration/', registration_page, name='registration_page'),
    path('login/', login_page, name='login_page'),
    path('blocked/', blocked_page, name='blocked_page'),

    # ПАРТНЕРКА
    path('partnership', partnership_page, name='partnership_page'),
    path('followers', followers_page, name='followers_page'),
    path('set_last_month/', set_last_month, name='set_last_month'),
    path('set_next_month/', set_next_month, name='set_next_month'),
    path('partnership_notification_close/', partnership_notification_close, name='partnership_notification_close'),

]
