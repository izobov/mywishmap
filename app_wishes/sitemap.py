from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class StaticViewSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.9
    protocol = 'https'

    def items(self):
        return ['homepage', 'landing_page', 'registration_page', 'login_page', ]

    def location(self, item):
        return reverse(item)
