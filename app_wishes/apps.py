from django.apps import AppConfig


class AppWishesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_wishes'
