import calendar
import datetime
import json
import logging
import os
import time
from datetime import date, timedelta
from random import choice, randint

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.timezone import utc
from PIL import Image
from yookassa import Configuration, Payment

logger = logging.getLogger(__name__)


class UserAction(models.Model):
    # date, email, action, information
    date = models.DateField(
        verbose_name='Дата',
        auto_now_add=True,
    )
    email = models.CharField(
        verbose_name='Почта',
        max_length=255,
        default='',
    )
    amount = models.PositiveIntegerField(
        verbose_name='Сумма',
        default=0,
    )
    information = models.CharField(
        verbose_name='Информация',
        max_length=255,
        default='',
    )

    objects = models.Manager()

    class Meta:
        db_table = 'user_actions'
        verbose_name = 'Пользовательская активность'
        verbose_name_plural = 'Пользовательская активность'
        ordering = ['date']

    def __str__(self):
        return f'{self.information} {self.amount}'


class Tariff(models.Model):
    title = models.CharField(
        verbose_name='Наименование',
        max_length=100,
        blank=True,
    )
    description = models.CharField(
        verbose_name='Описание',
        max_length=1024,
        blank=True,
    )
    period = models.PositiveIntegerField(
        verbose_name='Период в месяцах',
        default=0,
    )
    price = models.PositiveIntegerField(
        verbose_name='Цена за период',
        default=0,
    )
    is_renewable = models.BooleanField(
        verbose_name='Продляемый',
        default=True,
    )

    objects = models.Manager()

    class Meta:
        db_table = 'tariffs'
        verbose_name = 'Тариф'
        verbose_name_plural = 'Тарифы'
        ordering = ['period']

    def __str__(self):
        return self.title

    def get_payment_url(self):
        """Возвращает ссылку на страницу оплаты по тарифу"""
        return f'../payment/{self.pk}'


class Subscription(models.Model):
    is_active = models.BooleanField(
        verbose_name='Активна',
        default=False,
    )
    tariff = models.ForeignKey(
        Tariff,
        on_delete=models.SET_NULL,
        verbose_name='Тариф',
        blank=True,
        null=True,
        default=None,
    )
    expired_date = models.DateField(
        verbose_name='Дата окончания',
        blank=True,
        null=True,
        default=None,
    )
    renewal_date = models.DateField(
        verbose_name='Дата автопродления',
        blank=True,
        null=True,
        default=None,
    )
    autopay = models.BooleanField(
        verbose_name='Автоплатеж',
        default=False,
    )
    is_trial = models.BooleanField(
        # НЕ ИСПОЛЬЗУЕТСЯ
        verbose_name='Пробная подписка',
        default=False,
    )
    # ДАННЫЕ ПЛАТЕЖА
    payment_id = models.CharField(
        verbose_name='Идентификатор платежа',
        max_length=50,
        blank=True,
        null=True,
        default=None,
    )
    payment_method_id = models.CharField(
        verbose_name='Идентификатор способа оплаты',
        max_length=50,
        blank=True,
        null=True,
        default=None,
    )
    payment_status = models.CharField(
        verbose_name='Статус платежа',
        max_length=20,
        blank=True,
        null=True,
        default=None,
    )

    objects = models.Manager()

    class Meta:
        db_table = 'subscriptions'
        verbose_name = 'Подписка'
        verbose_name_plural = 'Подписки'

    def __str__(self):
        if self.is_active:
            if self.autopay:
                return f'{self.tariff} до {self.expired_date} с автопродлением'
            else:
                return f'{self.tariff} до {self.expired_date}'
        return 'Отсутствует'

    def autopay_change(self):
        """Если возможно, меняет состояние автоплатежей подписки на противоположное"""
        if self.autopay:
            self.autopay = False
            self.save()
        else:
            if self.payment_method_id:
                self.autopay = True
                self.save()

    def check_autopayment(self):
        """Контроль автоплатежей"""
        if self.is_active and self.autopay:
            if self.renewal_date <= self.expired_date:
                # Если renewal_date > expired_date значит сегодня запрос на проведение автоплатежа уже производился.
                # И не прошел.
                if self.payment_method_id:
                    if self.renewal_date <= date.today():
                        self.renewal_date += timedelta(1)

                        # Запрос на проведение платежа
                        payment_value = self.tariff.price
                        payment_currency = 'RUB'
                        payment_description = f'ВИЖН. Продление подписки – {self.tariff.title}'

                        Configuration.account_id = settings.YOOKASSA_ACCOUNT_ID
                        Configuration.secret_key = settings.YOOKASSA_SECRET_KEY

                        payment = dict(Payment.create({
                            "amount": {
                                "value": payment_value,
                                "currency": payment_currency,
                            },
                            "capture": True,
                            "payment_method_id": self.payment_method_id,
                            "description": payment_description,
                            
                            # --- ДАННЫЕ ДЛЯ ОТПРАВКИ ЧЕКА ---
                            "receipt": {
                                "customer": {
                                    "email": self.get_email_or_none(),
                                },
                                "items": [
                                    {
                                        "description": payment_description,
                                        "quantity": "1",
                                        "amount": {
                                            "value": payment_value,
                                            "currency": payment_currency,
                                        },
                                        "vat_code": "1"
                                    },
                                ]
                            }
                            # --- КОНЕЦ ---
                        }))

                        # Сохранение данных
                        self.payment_id = payment['id']
                        self.payment_status = payment['status']
                        # self.is_trial = False

                        # -----------------------------------

                        if payment['status'] == 'succeeded':
                            self.expired_date += timedelta(self.tariff.period * 31)
                            self.renewal_date = self.expired_date - timedelta(5)
                            self.is_active = True

                            if payment['payment_method']['saved']:
                                self.payment_method_id = payment['payment_method']['id']
                                self.autopay = self.tariff.is_renewable
                            else:
                                self.payment_method_id = None
                                self.autopay = False

                            # Запись журнала платежей
                            profile = self.get_profile_or_none()
                            payment_value = float(payment['amount']['value'])
                            profile.save_action(amount=payment_value, information='Продление')

                            self.save()
                            return 'autopayment succeeded'

                        # -----------------------------------

                        self.save()
                else:
                    # Отключаем автоплатежи
                    self.autopay = False
                    # self.is_trial = False
                    self.save()
                    return 'autopayment canceled'
        return None

    def check_end_of_subscription(self):
        """Проверяет окончание подписки"""
        result = None
        if self.is_active and self.expired_date:
            if datetime.date.today() > self.expired_date:
                self.tariff = None
                self.is_active = False
                self.expired_date = None
                self.renewal_date = None
                self.autopay = False
                self.payment_id = None
                self.payment_method_id = None
                self.payment_status = None
                self.save()
                result = 'subscription stopped'
        return result

    def check_pending_payment(self):
        """Контроль незавершенных платежей"""
        pending_payment_status = None
        if self.payment_id and self.payment_status == 'pending':

            # Запрос состояния платежа
            Configuration.account_id = settings.YOOKASSA_ACCOUNT_ID
            Configuration.secret_key = settings.YOOKASSA_SECRET_KEY
            payment_id = self.payment_id
            payment = dict(Payment.find_one(payment_id))
            payment_status = payment['status']
            
            # logger.info('Информация о платеже:')
            # logger.info(f'ID: {payment_id}')
            # logger.info(f'Статус: {payment_status}')

            # Статус платежа не изменился
            if payment_status == 'pending':
                return pending_payment_status

            # Платеж отменен
            if payment_status == 'canceled':
                self.payment_status = 'canceled'
                self.is_active = False

            # Платеж прошел
            if payment_status == 'succeeded':
                self.expired_date = date.today() + timedelta(self.tariff.period * 31)
                self.renewal_date = self.expired_date - timedelta(5)
                self.payment_status = payment['status']
                self.is_active = True
                if payment['payment_method']['saved']:
                    self.payment_method_id = payment['payment_method']['id']
                    self.autopay = self.tariff.is_renewable
                else:
                    self.payment_method_id = None
                    self.autopay = False
                self.is_active = True

                # Запись журнала платежей
                profile = self.get_profile_or_none()
                payment_value = float(payment['amount']['value'])
                profile.save_action(amount=payment_value, information='Оплата')

                pending_payment_status = 'payment succeeded'
            self.save()
        return pending_payment_status

    def check_subscription_status(self):
        autopayment_status = self.check_autopayment()
        # autopayment canceled      – автоплатежи отменены
        # autopayment succeeded     – автоплатеж прошел
        # None

        pending_payment_status = self.check_pending_payment()
        # payment succeeded         – платеж проведен
        # None

        subscription_status = self.check_end_of_subscription()
        # subscription stopped      – подписка остановлена
        # None

        if subscription_status:
            return subscription_status
        if pending_payment_status:
            return pending_payment_status
        if autopayment_status:
            return autopayment_status
        return None

    def get_email_or_none(self):
        profile = Profile.objects.filter(subscription=self).first()
        if profile:
            return profile.user.email
        return None

    def get_profile_or_none(self):
        return Profile.objects.filter(subscription=self).first()


class Profile(models.Model):

    user = models.OneToOneField(
        User,
        verbose_name='Пользователь',
        on_delete=models.CASCADE,
    )
    userpic = models.ImageField(
        verbose_name='Аватар',
        upload_to='images/%Y/%m/%d',
        blank=True,
        null=True,
        default=None,
    )
    leader = models.ForeignKey(
        'self',
        verbose_name='Лидер',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
    )
    subscription = models.OneToOneField(
        Subscription,
        verbose_name='Подписка',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
    )
    visionmap_mode = models.BooleanField(
        verbose_name='Режим вижн-карты',
        default=False,
    )
    is_partner = models.BooleanField(
        verbose_name='Партнер',
        default=False,
    )
    show_app_install_notification = models.BooleanField(
        verbose_name='Показывать уведомление об установке приложения',
        default=True,
    )
    show_wish_info_notification = models.BooleanField(
        verbose_name='Показывать уведомление об информации',
        default=True,
    )
    feedback_value = models.PositiveIntegerField(
        verbose_name='Оценка',
        default=0,
    )
    feedback_text = models.CharField(
        verbose_name='Отзыв',
        max_length=1000,
        blank=True,
        default='',
    )
    areas_dict = models.JSONField(
        verbose_name='Словарь областей',
        default=dict,
    )
    # areas_dict = {
    #     'a':  [area_pk, ],
    #     'b1': [area_pk, ],
    #     'b2': [area_pk, ],
    #     'c1': [area_pk, ],
    #     'c2': [area_pk, ],
    #     'c3': [area_pk, ],
    # }
    show_partnership_notification = models.BooleanField(
        verbose_name='Показывать уведомление о партнерстве',
        default=True,
    )
    information = models.TextField(
        verbose_name='Информация',
        default='',
    )
    active_month_start_date = models.DateField(
        verbose_name='Дата начала активного периода',
        default=datetime.date.today().replace(day=1),
    )

    objects = models.Manager()

    class Meta:
        db_table = 'profiles'
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профили пользователей'

    def __str__(self):
        if self.user.first_name:
            return self.user.first_name
        else:
            return self.user.username

    def save_action(self, amount, information):
        action = UserAction.objects.create(
            email=self.user.email,
            amount=amount,
            information=information,
        )
        action.save()

    def get_invite_code(self):
        symbols = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnPpQqRrSsTtUuVvWwXxYyZz'
        invite_code = choice(symbols)
        invite_code += choice(symbols)
        invite_code += str(randint(0, 9))
        invite_code += str(randint(0, 9))
        invite_code += choice(symbols)
        invite_code += choice(symbols)
        invite_code += str(self.pk)
        return invite_code

    def get_month_end_date(self, date):
        """Возвращает дату конца месяца"""
        days_count = self.get_month_days_count(date)
        end_date = date.replace(day=1) + datetime.timedelta(days=(days_count - 1))
        return end_date

    @classmethod
    def get_month_days_count(cls, date):
        """Возвращает количество дней в месяце"""
        return calendar.monthrange(date.year, date.month)[1]

    def set_last_month(self):
        """Устанавливает прошлый месяц в качестве активного"""
        start_date = self.active_month_start_date - datetime.timedelta(days=1)
        start_date = start_date.replace(day=1)
        self.active_month_start_date = start_date
        self.save()

    def set_next_month(self):
        """Устанавливает следующий месяц в качестве активного"""
        days_count = self.get_month_days_count(self.active_month_start_date)
        start_date = self.active_month_start_date + datetime.timedelta(days=days_count)
        self.active_month_start_date = start_date
        self.save()

    def wishes_count(self):
        wishes = Wish.objects.filter(profile=self, is_archived=False)
        return wishes.count()

    def actual_wishes(self):
        """Возвращает набор актуальных желаний"""
        actual_wishes = Wish.objects.filter(profile=self, is_archived=False)
        if actual_wishes.first():
            return actual_wishes
        return None

    def archived_wishes(self):
        """Возвращает набор исполненных желаний"""
        archived_wishes = Wish.objects.filter(profile=self, is_archived=True)
        if archived_wishes.first():
            return archived_wishes
        return None

    # TODO: Написать процедуру очистки словарей от мусора

    def area_create(self, title: str, color: str):
        """Создание области желаний и размещение в areas_dict"""
        wishes_dict = {'a': [], 'b1': [], 'b2': [], }
        area = Area.objects.create(
            profile=self,
            title=title,
            color=color,
            wishes_dict=json.dumps(wishes_dict)
        )
        areas_dict = json.loads(self.areas_dict)
        # ОДНА КОЛОНКА
        a_column_list = areas_dict['a']
        a_column_list.append(area.pk)
        areas_dict['a'] = a_column_list
        # ДВЕ КОЛОНКИ
        b1_column_list = areas_dict['b1']
        b1_column_list.append(area.pk)
        areas_dict['b1'] = b1_column_list
        # ТРИ КОЛОНКИ
        c1_column_list = areas_dict['c1']
        c1_column_list.append(area.pk)
        areas_dict['c1'] = c1_column_list
        self.areas_dict = json.dumps(areas_dict)
        self.save()
        return area

    def area_delete(self, pk):
        area = Area.objects.filter(pk=pk).first()
        if area:
            areas_dict = json.loads(self.areas_dict)
            for value in areas_dict.values():
                if pk in value:
                    value.remove(pk)
            self.areas_dict = json.dumps(areas_dict)
            self.save()
            area.delete()
            return True
        return False

    def areas(self):
        """Возвращает набор областей"""
        areas = Area.objects.filter(profile=self)
        if areas.first():
            return areas
        return None

    @classmethod
    def get_absolute_url(cls):
        return reverse('profile')

    @classmethod
    def get_app_install_notification_close_url(cls):
        """Сокрытие уведомления об установке приложения"""
        return reverse('app_install_notification_close')

    @classmethod
    def get_partnership_notification_close_url(cls):
        """Сокрытие уведомления об установке приложения"""
        return reverse('partnership_notification_close')

    def get_countdown(self):
        """Возвращает время прошедшее с момента регистрации или None если прошло более суток"""
        now = datetime.datetime.utcnow().replace(tzinfo=utc)
        countdown = now - self.user.date_joined
        if countdown.days > 1:
            return None
        return countdown

    @classmethod
    def get_userpic_delete_url(cls):
        return reverse('userpic_delete')

    def get_userpic_url(self):
        if self.userpic and hasattr(self.userpic, 'url'):
            return self.userpic.url
        else:
            return "/static/images/userpic.png"

    def initial_filling(self):
        """Заполнение данных при создании профиля"""
        self.subscription = Subscription.objects.create()
        wishes_dict = {
            'a': [],
            'b1': [],
            'b2': [],
        }
        column_a = []
        column_b1 = []
        column_b2 = []
        column_c1 = []
        column_c2 = []
        column_c3 = []
        # 1. Богатство и благополучие / лиловый – #D45981
        area = Area.objects.create(
            profile=self,
            title='Богатство и благополучие',
            color='#D45981',
            wishes_dict=json.dumps(wishes_dict)
        )
        column_a.append(area.pk)
        column_b1.append(area.pk)
        column_c1.append(area.pk)
        # 2. Любовь и отношения / розовый – #FF7EBC
        area = Area.objects.create(
            profile=self,
            title='Любовь и отношения',
            color='#FF7EBC',
            wishes_dict=json.dumps(wishes_dict)
        )
        column_a.append(area.pk)
        column_b2.append(area.pk)
        column_c2.append(area.pk)
        # 3. Бизнес и карьера / красный – #E74A4A
        area = Area.objects.create(
            profile=self,
            title='Бизнес и карьера',
            color='#E74A4A',
            wishes_dict=json.dumps(wishes_dict)
        )
        column_a.append(area.pk)
        column_b1.append(area.pk)
        column_c3.append(area.pk)
        # 4. Семья, дети / изумрудный – #50C878
        area = Area.objects.create(
            profile=self,
            title='Семья, дети',
            color='#50C878',
            wishes_dict=json.dumps(wishes_dict)
        )
        column_a.append(area.pk)
        column_b2.append(area.pk)
        column_c1.append(area.pk)
        # 5. Я и мое здоровье / оранжевый – #FF9110
        area = Area.objects.create(
            profile=self,
            title='Я и мое здоровье',
            color='#FF9110',
            wishes_dict=json.dumps(wishes_dict)
        )
        column_a.append(area.pk)
        column_b1.append(area.pk)
        column_c2.append(area.pk)
        # 6. Знания и мудрость / бежевый – #FFCA8B
        area = Area.objects.create(
            profile=self,
            title='Знания и мудрость',
            color='#FFCA8B',
            wishes_dict=json.dumps(wishes_dict)
        )
        column_a.append(area.pk)
        column_b2.append(area.pk)
        column_c3.append(area.pk)
        # 7. Хобби и творчество / серый – #9D9D9D
        area = Area.objects.create(
            profile=self,
            title='Хобби и творчество',
            color='#9D9D9D',
            wishes_dict=json.dumps(wishes_dict)
        )
        column_a.append(area.pk)
        column_b1.append(area.pk)
        column_c1.append(area.pk)
        # 8. Друзья и окружение / голубой – #5BB3FB
        area = Area.objects.create(
            profile=self,
            title='Друзья и окружение',
            color='#5BB3FB',
            wishes_dict=json.dumps(wishes_dict)
        )
        column_a.append(area.pk)
        column_b2.append(area.pk)
        column_c2.append(area.pk)
        # 9. Путешествия, отдых и развлечения / желтый – #FED500
        area = Area.objects.create(
            profile=self,
            title='Путешествия, отдых и развлечения',
            color='#FED500',
            wishes_dict=json.dumps(wishes_dict)
        )
        column_a.append(area.pk)
        column_b1.append(area.pk)
        column_c3.append(area.pk)
        areas_dict = {
            'a': column_a,
            'b1': column_b1,
            'b2': column_b2,
            'c1': column_c1,
            'c2': column_c2,
            'c3': column_c3,
        }
        self.areas_dict = json.dumps(areas_dict)
        self.save()

    @classmethod
    def tariffs(cls):
        """Возвращает набор тарифов"""
        tariffs = Tariff.objects.all()
        if tariffs.first():
            return tariffs
        return None

    def userpic_delete(self):
        if self.userpic and hasattr(self.userpic, 'path'):
            if os.path.exists(self.userpic.path):
                os.remove(self.userpic.path)
            self.userpic.delete(save=True)

    def userpic_transformation(self):
        if self.userpic and hasattr(self.userpic, 'path'):
            image_path = self.userpic.path
            temp_image = Image.open(image_path)

            # Изменение ориентации изображения
            exif_data = temp_image._getexif()
            if exif_data:
                for key, val in exif_data.items():
                    if key == 274:
                        print(val)
                        if val == 3:
                            temp_image = temp_image.rotate(180, expand=True)
                        elif val == 6:
                            temp_image = temp_image.rotate(270, expand=True)
                        elif val == 8:
                            temp_image = temp_image.rotate(90, expand=True)

            width, height = temp_image.size
            ratio = width / height
            if width >= height:
                if height > settings.USERPIC_MAX_WIDTH:
                    height = settings.USERPIC_MAX_WIDTH
                    width = round(height * ratio)
                    temp_image = temp_image.resize((width, height), Image.ANTIALIAS)
                delta = round((width - height) / 2)
                crop_box = (delta, 0, delta + height, height)
                temp_image = temp_image.crop(crop_box)
                temp_image.save(image_path, quality=settings.USERPIC_QUALITY)
            else:
                if width > settings.USERPIC_MAX_WIDTH:
                    width = settings.USERPIC_MAX_WIDTH
                    height = round(width / ratio)
                    temp_image = temp_image.resize((width, height), Image.ANTIALIAS)
                delta = round((height - width) / 2)
                crop_box = (0, delta, width, delta + width)
                temp_image = temp_image.crop(crop_box)
                temp_image.save(image_path, quality=settings.USERPIC_QUALITY)

    # ПОЛУЧЕНИЕ СПИСКОВ С ОБЛАСТЯМИ ====================================================================
    def a_column_areas(self):
        """Возвращает набор областей в соответствии с a_column_list"""
        areas_dict = json.loads(self.areas_dict)
        a_column_areas = []
        for area_pk in areas_dict['a']:
            area = Area.objects.filter(pk=area_pk).first()
            if area:
                a_column_areas.append(area)
        return a_column_areas

    def b1_column_areas(self):
        """Возвращает набор областей в соответствии с b1_column_list"""
        areas_dict = json.loads(self.areas_dict)
        b1_column_areas = []
        for area_pk in areas_dict['b1']:
            area = Area.objects.filter(pk=area_pk).first()
            if area:
                b1_column_areas.append(area)
        return b1_column_areas

    def b2_column_areas(self):
        """Возвращает набор областей в соответствии с b2_column_list"""
        areas_dict = json.loads(self.areas_dict)
        b2_column_areas = []
        for area_pk in areas_dict['b2']:
            area = Area.objects.filter(pk=area_pk).first()
            if area:
                b2_column_areas.append(area)
        return b2_column_areas

    def c1_column_areas(self):
        """Возвращает набор областей в соответствии с c1_column_list"""
        areas_dict = json.loads(self.areas_dict)
        c1_column_areas = []
        for area_pk in areas_dict['c1']:
            area = Area.objects.filter(pk=area_pk).first()
            if area:
                c1_column_areas.append(area)
        return c1_column_areas

    def c2_column_areas(self):
        """Возвращает набор областей в соответствии с c2_column_list"""
        areas_dict = json.loads(self.areas_dict)
        c2_column_areas = []
        for area_pk in areas_dict['c2']:
            area = Area.objects.filter(pk=area_pk).first()
            if area:
                c2_column_areas.append(area)
        return c2_column_areas

    def c3_column_areas(self):
        """Возвращает набор областей в соответствии с c3_column_list"""
        areas_dict = json.loads(self.areas_dict)
        c3_column_areas = []
        for area_pk in areas_dict['c3']:
            area = Area.objects.filter(pk=area_pk).first()
            if area:
                c3_column_areas.append(area)
        return c3_column_areas


class Area(models.Model):
    profile = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        verbose_name='Профиль',
        blank=True,
        null=True,
        default=None,
    )
    title = models.CharField(
        verbose_name='Наименование',
        max_length=100,
    )
    color = models.CharField(
        verbose_name='Цвет',
        max_length=7,
    )
    is_double = models.BooleanField(
        verbose_name='В две колонки',
        default=True,
    )
    wishes_dict = models.JSONField(
        verbose_name='Словарь желаний',
        default=dict,
    )
    # wishes_dict = {
    #     'a':  [wish_pk, ],
    #     'b1': [wish_pk, ],
    #     'b2': [wish_pk, ],
    # }

    objects = models.Manager()

    class Meta:
        db_table = 'areas'
        verbose_name = 'Область желаний'
        verbose_name_plural = 'Области желаний'

    def __str__(self):
        return self.title

    def actual_wishes(self):
        actual_wishes = Wish.objects.filter(area=self, is_archived=False)
        if actual_wishes.first():
            return actual_wishes
        return None

    def get_change_cols_count_url(self):
        """Изменение количества колонок желаний"""
        return reverse('area_change_cols_count', kwargs={'pk': self.pk})

    def get_delete_url(self):
        """Удаление элемента"""
        return reverse('area_delete', kwargs={'pk': self.pk})

    def wish_create(self, title: str):
        """Создание желания и размещение в wishes_dict"""
        wish = Wish.objects.create(profile=self.profile, area=self, title=title)
        wishes_dict = json.loads(self.wishes_dict)
        # ОДНА КОЛОНКА
        a_column_list = wishes_dict['a']
        a_column_list.append(wish.pk)
        wishes_dict['a'] = a_column_list
        # ДВЕ КОЛОНКИ
        b1_column_list = wishes_dict['b1']
        b2_column_list = wishes_dict['b2']
        if len(b1_column_list) <= len(b2_column_list):
            b1_column_list.append(wish.pk)
            wishes_dict['b1'] = b1_column_list
        else:
            b2_column_list.append(wish.pk)
            wishes_dict['b2'] = b2_column_list
        self.wishes_dict = json.dumps(wishes_dict)
        self.save()
        return wish

    def wish_delete(self, pk):
        wish = Wish.objects.filter(pk=pk).first()
        if wish:
            wishes_dict = json.loads(self.wishes_dict)
            for value in wishes_dict.values():
                if pk in value:
                    value.remove(pk)
            self.wishes_dict = json.dumps(wishes_dict)
            self.save()
            wish.image_delete()
            wish.delete()
            return True
        return False

    # ПОЛУЧЕНИЕ СПИСКОВ С ЖЕЛАНИЯМИ ====================================================================
    def a_column_wishes(self):
        """Возвращает набор желаний в соответствии с a_column_list"""
        wishes_dict = json.loads(self.wishes_dict)
        a_column_wishes = []
        for wish_pk in wishes_dict['a']:
            wish = Wish.objects.filter(pk=wish_pk, is_archived=False).first()
            if wish:
                a_column_wishes.append(wish)
        return a_column_wishes

    def b1_column_wishes(self):
        """Возвращает набор желаний в соответствии с b1_column_list"""
        wishes_dict = json.loads(self.wishes_dict)
        b1_column_wishes = []
        for wish_pk in wishes_dict['b1']:
            wish = Wish.objects.filter(pk=wish_pk, is_archived=False).first()
            if wish:
                b1_column_wishes.append(wish)
        return b1_column_wishes

    def b2_column_wishes(self):
        """Возвращает набор желаний в соответствии с b2_column_list"""
        wishes_dict = json.loads(self.wishes_dict)
        b2_column_wishes = []
        for wish_pk in wishes_dict['b2']:
            wish = Wish.objects.filter(pk=wish_pk, is_archived=False).first()
            if wish:
                b2_column_wishes.append(wish)
        return b2_column_wishes

    # СДВИГ ОБЛАСТИ ====================================================================================
    # ОДНА КОЛОНКА
    def can_move_up_in_column_a(self):
        areas_dict = json.loads(self.profile.areas_dict)
        a_column_list = list(areas_dict['a'])
        if a_column_list[0] != self.pk:
            return True
        return False

    def can_move_down_in_column_a(self):
        areas_dict = json.loads(self.profile.areas_dict)
        a_column_list = list(areas_dict['a'])
        if a_column_list[-1] != self.pk:
            return True
        return False

    def get_move_up_in_column_a_url(self):
        """Сдвиг элемента вверх в колонке A"""
        return reverse('area_move_up', kwargs={'pk': self.pk, 'column': 'a'})

    def get_move_down_in_column_a_url(self):
        """Сдвиг элемента вниз в колонке A"""
        return reverse('area_move_down', kwargs={'pk': self.pk, 'column': 'a'})

    # ДВЕ КОЛОНКИ
    # КОЛОНКА B1
    def can_move_up_in_column_b1(self):
        areas_dict = json.loads(self.profile.areas_dict)
        b1_column_list = list(areas_dict['b1'])
        if b1_column_list[0] != self.pk:
            return True
        return False

    def can_move_down_in_column_b1(self):
        areas_dict = json.loads(self.profile.areas_dict)
        b1_column_list = list(areas_dict['b1'])
        if b1_column_list[-1] != self.pk:
            return True
        return False

    def get_move_up_in_column_b1_url(self):
        """Сдвиг элемента вверх в колонке B1"""
        return reverse('area_move_up', kwargs={'pk': self.pk, 'column': 'b1'})

    def get_move_down_in_column_b1_url(self):
        """Сдвиг элемента вниз в колонке B1"""
        return reverse('area_move_down', kwargs={'pk': self.pk, 'column': 'b1'})

    def get_move_right_in_column_b1_url(self):
        """Перенос элемента из колонки B1 в колонку B2"""
        return reverse('area_move_right', kwargs={'pk': self.pk, 'column': 'b1'})

    # КОЛОНКА B2
    def can_move_up_in_column_b2(self):
        areas_dict = json.loads(self.profile.areas_dict)
        b2_column_list = list(areas_dict['b2'])
        if b2_column_list[0] != self.pk:
            return True
        return False

    def can_move_down_in_column_b2(self):
        areas_dict = json.loads(self.profile.areas_dict)
        b2_column_list = list(areas_dict['b2'])
        if b2_column_list[-1] != self.pk:
            return True
        return False

    def get_move_up_in_column_b2_url(self):
        """Сдвиг элемента вверх в колонке B2"""
        return reverse('area_move_up', kwargs={'pk': self.pk, 'column': 'b2'})

    def get_move_down_in_column_b2_url(self):
        """Сдвиг элемента вниз в колонке B2"""
        return reverse('area_move_down', kwargs={'pk': self.pk, 'column': 'b2'})

    def get_move_left_in_column_b2_url(self):
        """Перенос элемента из колонки B2 в колонку B1"""
        return reverse('area_move_left', kwargs={'pk': self.pk, 'column': 'b2'})

    # ТРИ КОЛОНКИ
    # КОЛОНКА С1
    def can_move_up_in_column_c1(self):
        areas_dict = json.loads(self.profile.areas_dict)
        c1_column_list = list(areas_dict['c1'])
        if c1_column_list[0] != self.pk:
            return True
        return False

    def can_move_down_in_column_c1(self):
        areas_dict = json.loads(self.profile.areas_dict)
        c1_column_list = list(areas_dict['c1'])
        if c1_column_list[-1] != self.pk:
            return True
        return False

    def get_move_up_in_column_c1_url(self):
        """Сдвиг элемента вверх в колонке C1"""
        return reverse('area_move_up', kwargs={'pk': self.pk, 'column': 'c1'})

    def get_move_down_in_column_c1_url(self):
        """Сдвиг элемента вниз в колонке C1"""
        return reverse('area_move_down', kwargs={'pk': self.pk, 'column': 'c1'})

    def get_move_right_in_column_c1_url(self):
        """Перенос элемента из колонки C1 в колонку C2"""
        return reverse('area_move_right', kwargs={'pk': self.pk, 'column': 'c1'})

    # КОЛОНКА С2
    def can_move_up_in_column_c2(self):
        areas_dict = json.loads(self.profile.areas_dict)
        c2_column_list = list(areas_dict['c2'])
        if c2_column_list[0] != self.pk:
            return True
        return False

    def can_move_down_in_column_c2(self):
        areas_dict = json.loads(self.profile.areas_dict)
        c2_column_list = list(areas_dict['c2'])
        if c2_column_list[-1] != self.pk:
            return True
        return False

    def get_move_up_in_column_c2_url(self):
        """Сдвиг элемента вверх в колонке C2"""
        return reverse('area_move_up', kwargs={'pk': self.pk, 'column': 'c2'})

    def get_move_down_in_column_c2_url(self):
        """Сдвиг элемента вниз в колонке C2"""
        return reverse('area_move_down', kwargs={'pk': self.pk, 'column': 'c2'})

    def get_move_left_in_column_c2_url(self):
        """Перенос элемента из колонки C2 в колонку C1"""
        return reverse('area_move_left', kwargs={'pk': self.pk, 'column': 'c2'})

    def get_move_right_in_column_c2_url(self):
        """Перенос элемента из колонки C2 в колонку C3"""
        return reverse('area_move_right', kwargs={'pk': self.pk, 'column': 'c2'})

    # КОЛОНКА С3
    def can_move_up_in_column_c3(self):
        areas_dict = json.loads(self.profile.areas_dict)
        c3_column_list = list(areas_dict['c3'])
        if c3_column_list[0] != self.pk:
            return True
        return False

    def can_move_down_in_column_c3(self):
        areas_dict = json.loads(self.profile.areas_dict)
        c3_column_list = list(areas_dict['c3'])
        if c3_column_list[-1] != self.pk:
            return True
        return False

    def get_move_up_in_column_c3_url(self):
        """Сдвиг элемента вверх в колонке C3"""
        return reverse('area_move_up', kwargs={'pk': self.pk, 'column': 'c3'})

    def get_move_down_in_column_c3_url(self):
        """Сдвиг элемента вниз в колонке C3"""
        return reverse('area_move_down', kwargs={'pk': self.pk, 'column': 'c3'})

    def get_move_left_in_column_c3_url(self):
        """Перенос элемента из колонки C3 в колонку C2"""
        return reverse('area_move_left', kwargs={'pk': self.pk, 'column': 'c3'})


class Wish(models.Model):
    profile = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        verbose_name='Профиль',
        blank=True,
        null=True,
        default=None,
    )
    area = models.ForeignKey(
        Area,
        on_delete=models.CASCADE,
        verbose_name='Область желаний',
    )
    title = models.CharField(
        verbose_name='Наименование',
        max_length=1000,
    )
    image = models.ImageField(
        verbose_name='Изображение',
        upload_to='images/%Y/%m/%d',
        default='',
    )
    details = models.TextField(
        verbose_name='Подробности',
        default='',
    )
    context = models.TextField(
        verbose_name='Осознание',
        default='',
    )
    representation = models.TextField(
        verbose_name='Воспоминания',
        default='',
    )
    changes = models.TextField(
        verbose_name='Изменения',
        default='',
    )
    information = models.TextField(
        verbose_name='Информация',
        default='',
    )
    importance = models.PositiveIntegerField(
        verbose_name='Важность',
        default=0,
    )
    progress = models.PositiveIntegerField(
        verbose_name='Прогресс',
        default=0,
    )
    is_accepted = models.BooleanField(
        verbose_name='Принятие ответственности',
        default=False,
    )
    is_archived = models.BooleanField(
        verbose_name='В архиве',
        default=False,
    )
    show_checked_todoes = models.BooleanField(
        verbose_name='Показывать завершенные действия',
        default=True,
    )

    objects = models.Manager()

    class Meta:
        db_table = 'wishes'
        verbose_name = 'Желание'
        verbose_name_plural = 'Желания'
        ordering = ['area']

    def __str__(self):
        return self.title

    def fears(self):
        fears = Fear.objects.filter(wish=self)
        if fears.first():
            return fears
        return None

    def get_absolute_url(self):
        return reverse('wish_page', kwargs={'pk': self.pk})

    def get_change_archiving_url(self):
        return reverse('wish_change_archiving', kwargs={'pk': self.pk})

    def get_change_checked_todoes_view_url(self):
        return reverse('wish_change_checked_todoes_view', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse('wish_delete', kwargs={'pk': self.pk})

    def get_fears_list_url(self):
        return reverse('fears_list', kwargs={'pk': self.pk})

    def get_image_delete_url(self):
        return reverse('wish_image_delete', kwargs={'pk': self.pk})

    def get_image_url_or_none(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
        else:
            return None

    def get_wish_info_notification_close_url(self):
        """Сокрытие уведомления об информации"""
        return reverse('wish_info_notification_close', kwargs={'pk': self.pk})

    def image_delete(self):
        if self.image and hasattr(self.image, 'path'):
            if os.path.exists(self.image.path):
                os.remove(self.image.path)
            self.image.delete(save=True)

    def image_transformation(self):
        image_path = self.image.path
        temp_image = Image.open(image_path)
        # Изменение ориентации изображения
        exif_data = temp_image._getexif()
        if exif_data:
            for key, val in exif_data.items():
                if key == 274:
                    print(val)
                    if val == 3:
                        temp_image = temp_image.rotate(180, expand=True)
                    elif val == 6:
                        temp_image = temp_image.rotate(270, expand=True)
                    elif val == 8:
                        temp_image = temp_image.rotate(90, expand=True)
        width, height = temp_image.size
        ratio = width / height
        width = settings.WISH_IMAGE_MAX_WIDTH
        height = round(width / ratio)
        temp_image = temp_image.resize((width, height), Image.ANTIALIAS)
        temp_image.save(image_path, quality=settings.WISH_IMAGE_QUALITY)

    def set_progress(self):
        value = 10
        max_value = 70
        if self.image:
            value += 10
        if self.details:
            value += 10
        if self.context:
            value += 10
        if self.representation:
            value += 10
        if self.changes:
            value += 10
        if self.is_accepted:
            value += 10
        # neutralized_fears = Fear.objects.filter(wish=self, neutralized=True)
        # if neutralized_fears.first():
        #     value += 10
        self.progress = round(value / max_value * 100)
        self.save()

    def todoes(self):
        todoes = ToDo.objects.filter(wish=self).order_by('pk')
        if todoes.first():
            return todoes
        return None

    def unchecked_todoes(self):
        unchecked_todoes = ToDo.objects.filter(wish=self, is_checked=False).order_by('pk')
        if unchecked_todoes.first():
            return unchecked_todoes
        return None


class Fear(models.Model):
    title = models.CharField(
        verbose_name='Наименование',
        max_length=200,
    )
    wish = models.ForeignKey(
        Wish,
        on_delete=models.CASCADE,
        verbose_name='Желание',
    )
    neutralized = models.BooleanField(
        verbose_name='Нейтрализовано',
        default=False,
    )

    objects = models.Manager()

    class Meta:
        db_table = 'fears'
        verbose_name = 'Страх'
        verbose_name_plural = 'Страхи'

    def __str__(self):
        return self.title

    def check_neutralizations_count(self):
        """Проверка и создание нейтрализаций"""
        preventions_count = Neutralization.objects.filter(fear=self, n_type=0).count()
        while preventions_count < 5:
            Neutralization.objects.create(fear=self, n_type=0)
            preventions_count = Neutralization.objects.filter(fear=self, n_type=0).count()
        eliminations_count = Neutralization.objects.filter(fear=self, n_type=1).count()
        while eliminations_count < 5:
            Neutralization.objects.create(fear=self, n_type=1)
            eliminations_count = Neutralization.objects.filter(fear=self, n_type=1).count()

    # def set_neutralization_status(self):
    #     prevented = False
    #     preventions = Neutralization.objects.filter(fear=self, n_type=0)
    #     for _ in preventions:
    #         if _.title != '':
    #             prevented = True
    #     eliminated = False
    #     eliminations = Neutralization.objects.filter(fear=self, n_type=1)
    #     for _ in eliminations:
    #         if _.title != '':
    #             eliminated = True
    #     self.neutralized = False
    #     if prevented and eliminated:
    #         self.neutralized = True
    #     self.save()

    def get_absolute_url(self):
        """Экран элемента"""
        return reverse('fear_page', kwargs={'pk': self.pk})

    def get_delete_url(self):
        """Удаление элемента"""
        return reverse('fear_delete', kwargs={'pk': self.pk})


class Neutralization(models.Model):
    title = models.CharField(
        verbose_name='Наименование',
        max_length=100,
        blank=True,
        null=True,
        default=None,
    )
    fear = models.ForeignKey(
        Fear,
        on_delete=models.CASCADE,
        verbose_name='Страх',
    )
    n_type = models.PositiveIntegerField(
        verbose_name='Тип нейтрализации',
        default=0,
        # 0 - предотвращения, 1 - устранения
    )

    objects = models.Manager()

    class Meta:
        db_table = 'neutralizations'
        verbose_name = 'Обезвреживания страхов'
        verbose_name_plural = 'Обезвреживание страхов'

    def __str__(self):
        if self.title:
            return self.title
        return 'Пустой объект'


class ToDo(models.Model):
    wish = models.ForeignKey(
        Wish,
        on_delete=models.CASCADE,
        verbose_name='Желание',
    )
    position = models.FloatField(
        verbose_name='Позиция',
        default=1234,
    )
    # TODO: Обдумать вопрос с позициями тудушек
    title = models.CharField(
        verbose_name='Наименование',
        max_length=200,
    )
    current_value = models.PositiveIntegerField(
        verbose_name='Текущее значение',
        default=0,
    )
    required_value = models.PositiveIntegerField(
        verbose_name='Требуемое значение',
        default=0,
    )
    is_checked = models.BooleanField(
        verbose_name='Выполнено',
        default=False,
    )

    objects = models.Manager()

    class Meta:
        db_table = 'todoes'
        verbose_name = 'Действие'
        verbose_name_plural = 'Действия'

    def __str__(self):
        return self.title

    def get_change_checked_url(self):
        return reverse('todo_change_checked', kwargs={'pk': self.pk})

    def get_rise_value_url(self):
        return reverse('todo_rise_value', kwargs={'pk': self.pk})

    def is_counter(self):
        if self.required_value != 0:
            return True
        return False


class Promocode(models.Model):
    promocode = models.CharField(
        verbose_name='Наименование',
        max_length=20,
        blank=True,
    )
    leader = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        verbose_name='Владелец',
        blank=True,
        null=True,
        default=None,
    )
    discount = models.PositiveIntegerField(
        verbose_name='Скидка, %',
        default=0,
    )

    objects = models.Manager()

    class Meta:
        db_table = 'promocodes'
        verbose_name = 'Промокод'
        verbose_name_plural = 'Промокоды'

    def __str__(self):
        return self.promocode



# class LeadersJournalRecord(models.Model):
#     leader = models.ForeignKey(
#         Profile,
#         on_delete=models.CASCADE,
#         verbose_name='Лидер',
#         blank=True,
#         null=True,
#         default=None,
#     )
#     date = models.DateTimeField(
#         verbose_name='Дата операции',
#         blank=True,
#         null=True,
#         default=None,
#     )
#     amount = models.IntegerField(
#         verbose_name='Сумма',
#         default=0,
#     )
#
#     objects = models.Manager()
#
#     class Meta:
#         db_table = 'promocodes'
#         verbose_name = 'Запись'
#         verbose_name_plural = 'Записи'
#
#     def __str__(self):
#         return self.title