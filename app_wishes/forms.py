from django import forms
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.template.loader import render_to_string

from app_wishes.models import Promocode


class InviteRegistrationForm(forms.Form):
    username = forms.EmailField(
        label='Электронная почта',
        widget=forms.EmailInput(attrs={"class": "form-control", "placeholder": ""})
    )
    first_name = forms.CharField(
        label='Ваше имя',
        widget=forms.TextInput(attrs={"class": "form-control", "placeholder": ""})
    )


class AreaForm(forms.Form):
    title = forms.CharField(
        label='Наименование области',
        max_length=100,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    color = forms.CharField(
        label='Цвет',
        widget=forms.TextInput(attrs={'class': 'form-control form-control-color w-25', 'type': 'color'})
    )


class ChangePasswordForm(PasswordChangeForm):
    old_password = forms.CharField(
        label='Старый пароль',
        strip=False,
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": ""}),
    )
    new_password1 = forms.CharField(
        label='Новый пароль',
        strip=False,
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": ""}),
    )
    new_password2 = forms.CharField(
        label='Повторите новый пароль',
        strip=False,
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": ""}),
    )


class FearTitleForm(forms.Form):
    title = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '5'})
    )


class FeedbackForm(forms.Form):
    text = forms.CharField(
        label='',
        required=False,
        widget=forms.Textarea(attrs={"class": "form-control", "rows": "5"})
    )


class LoginForm(AuthenticationForm):
    username = forms.EmailField(
        label='Электронная почта',
        widget=forms.EmailInput(attrs={"class": "form-control", "placeholder": ""})
    )
    password = forms.CharField(
        label='Пароль',
        strip=False,
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": ""})
    )


class NeutralizationTitleFormsetForm(forms.Form):
    title = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={'class': 'form-control mt-1', 'rows': '2'})
    )


class PromocodeForm(forms.Form):
    promocode = forms.CharField(
        label='Промокод',
        widget=forms.TextInput(attrs={'class': 'form-control', "placeholder": ""})
    )

    # def clean_promocode(self):
    #     promocode = self.cleaned_data.get('promocode')
    #     promocode = Promocode.objects.filter(promocode=promocode)
    #     if promocode.first():
    #         return self.cleaned_data.get('promocode')
    #     raise ValidationError('ПРОМОКОД НЕ НАЙДЕН')


# TODO: Подключить и протестировать проверку почтового ящика
class RegistrationForm(UserCreationForm):
    username = forms.EmailField(
        label='Электронная почта',
        widget=forms.EmailInput(attrs={"class": "form-control", "placeholder": ""})
    )
    first_name = forms.CharField(
        label='Ваше имя',
        widget=forms.TextInput(attrs={"class": "form-control", "placeholder": ""})
    )
    password1 = forms.CharField(
        label='Пароль',
        strip=False,
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": ""})
    )
    password2 = forms.CharField(
        label='Повторите пароль',
        strip=False,
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": ""})
    )

    class Meta:
        model = User
        fields = ['username', 'first_name', 'password1', 'password2']

    # def clean_username(self):
    #     subject = 'Добро пожаловать в приложение ВИЖН'
    #     plain_message = 'Добро пожаловать в приложение ВИЖН'
    #     email_from = settings.EMAIL_HOST_USER
    #     email_to = str(self.cleaned_data.get('username')).strip()
    #     context = {}
    #     html_message = render_to_string('app_users/mail__registration.html', context)
    #     result = send_mail(
    #         subject,
    #         plain_message,
    #         email_from,
    #         [email_to, ],
    #         html_message=html_message,
    #         fail_silently=True
    #     )
    #     if result == 0:
    #         raise ValidationError('Почтовый ящик недоступен')
    #     return email_to
        
        # email = self.cleaned_data.get('username')
        # subject = 'Вы зарегистрированы в сервисе ВИЖН'
        # message = 'Вы зарегистрированы в сервисе ВИЖН'
        # result = send_mail(subject, message, settings.EMAIL_HOST_USER, [email, ], fail_silently=True)
        # if result == 0:
        #     raise ValidationError('Почтовый ящик недоступен')
        # else:
        #     return email


class ResetConfirmPasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(
        label='Новый пароль',
        strip=False,
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": ""}),
    )
    new_password2 = forms.CharField(
        label='Повторите новый пароль',
        strip=False,
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": ""}),
    )


# TODO: Подключить и протестировать проверку почтового ящика
class ResetPasswordForm(PasswordResetForm):
    email = forms.EmailField(
        label='Электронная почта',
        max_length=254,
        widget=forms.EmailInput(attrs={"class": "form-control", "placeholder": ""})
    )

    # TODO: Подключить и протестировать проверку почтового ящика
    # def clean_email(self):
    #     email = User.objects.filter(email=self.cleaned_data.get('email')).first()
    #     if email:
    #         return email
    #     else:
    #         raise ValidationError('Указанный почтовый ящик не зарегистрирован')


class TodoForm(forms.Form):
    title = forms.CharField(
        label='Наименование',
        required=True,
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3})
    )
    current_value = forms.IntegerField(
        label='Текущее значение',
        required=False,
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'inputmode': 'numeric'
        }),
    )
    required_value = forms.IntegerField(
        label='Требуемое значение',
        required=False,
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'inputmode': 'numeric'
        }),
    )


class UsernameForm(forms.Form):
    first_name = forms.CharField(
        label='Ваше имя',
        widget=forms.TextInput(attrs={"class": "form-control"})
    )


class UserpicForm(forms.Form):
    image = forms.ImageField(
        label='Загрузить',
        required=False,
        widget=forms.FileInput(attrs={'class': 'form-control'})
    )
    url = forms.CharField(
        label='Загрузить по ссылке',
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'}))


class WishAcceptanceForm(forms.Form):
    is_accepted = forms.BooleanField(
        label='',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'onchange': 'document.getElementById(\'acceptance_button\').disabled = !this.checked;'})
    )


class WishChangesForm(forms.Form):
    changes = forms.CharField(
        label='',
        required=False,
        widget=forms.Textarea(attrs={'class': 'form-control mt-1', 'rows': 10})
    )


class WishContextForm(forms.Form):
    context = forms.CharField(
        label='',
        required=False,
        widget=forms.Textarea(attrs={'class': 'form-control mt-1', 'rows': 10})
    )


class WishDetailsForm(forms.Form):
    details = forms.CharField(
        label='',
        required=False,
        widget=forms.Textarea(attrs={'class': 'form-control mt-1', 'rows': 10})
    )


class WishImageForm(forms.Form):
    image = forms.ImageField(
        label='Загрузить из файла',
        required=False,
        widget=forms.FileInput(attrs={'class': 'form-control'})
    )
    url = forms.CharField(
        label='Загрузить по ссылке',
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )


class WishRepresentationForm(forms.Form):
    representation = forms.CharField(
        label='',
        required=False,
        widget=forms.Textarea(attrs={'class': 'form-control mt-1', 'rows': 10})
    )


class WishTitleForm(forms.Form):
    title = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={'class': 'form-control mt-1', 'rows': '10'})
    )
